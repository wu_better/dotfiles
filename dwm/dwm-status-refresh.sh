#!/bin/bash
print_battery () {
    # Change BAT1 to whatever your battery is identified as. Typically BAT0 or BAT1
    CHARGE=$(cat /sys/class/power_supply/BAT0/capacity)
    STATUS=$(cat /sys/class/power_supply/BAT0/status)
		ICONS=""
		if [ "$CHARGE" -gt 0 ] && [ "$CHARGE" -le 33 ]; then
			[ "$STATUS" = "Charging" ] && ICONS="" ||ICONS=""
		elif [ "$CHARGE" -gt 33 ] && [ "$CHARGE" -le 66 ]; then
			[ "$STATUS" = "Charging" ] && ICONS="" || ICONS=""
		elif [ "$CHARGE" -gt 66 ] && [ "$CHARGE" -le 98 ]; then
			[ "$STATUS" = "Charging" ] && ICONS="" || ICONS=""
		else 
		  [ "$STATUS" = "Charging" ] && ICONS="" || ICONS=""
    fi
	  echo "${ICONS} ${CHARGE}%"
}
print_volume() {
	base="$(amixer get Master | tail -n1)"
	volume="$(echo $base | sed -r 's/.*\[(.*)%\].*/\1/')"
	status="$(echo $base | sed -r 's/.*\[(on|off)\].*/\1/')"
	if [ "$status" = "off" ]; then
			echo "ﱝ OFF"
	elif [ "$volume" -gt 0 ] && [ "$volume" -le 33 ]; then
			echo " ${volume}%"
	elif [ "$volume" -gt 33 ] && [ "$volume" -le 66 ]; then
			echo "墳 ${volume}%"
	else
			echo " ${volume}%"
	fi
}

print_mem(){
	#memfree=$(($(grep -m1 'MemAvailable:' /proc/meminfo | awk '{print $2}') / 1024))
	totalmem=$(grep -m1 'MemTotal:' /proc/meminfo | awk '{print $2}') 
	memfree=$(grep -m1 'MemAvailable:' /proc/meminfo | awk '{print $2}')
	usedmem=$(((${totalmem} - ${memfree}) / 1024))
	pre="$((100 - $memfree * 100 / $totalmem))%"
	if [ "$usedmem" -gt 1024 ]; then
	  usedmem=$(echo "scale=2;${usedmem} / 1024" | bc)
	  usedmem="${usedmem}G"
	else
	  usedmem="${usedmem}M"
	fi
	echo -e " $usedmem/$pre"
}

print_temp(){
	test -f /sys/class/thermal/thermal_zone0/temp || return 0
	echo " $(head -c 2 /sys/class/thermal/thermal_zone0/temp)°C"
}

print_date(){
	# date "+  %m-%d  %H:%M 周%a"
	date "+ %m-%d/%H:%M"
}
print_cpu(){
  #脚本功能描述：依据/proc/stat文件获取并计算CPU使用率
  #
  #CPU时间计算公式：CPU_TIME=user+system+nice+idle+iowait+irq+softirq
  #CPU使用率计算公式：cpu_usage=(idle2-idle1)/(cpu2-cpu1)*100

  #默认时间间隔
  # TIME_INTERVAL=5
  # time=$(date "+%Y-%m-%d %H:%M:%S")
  # LAST_CPU_INFO=$(cat /proc/stat | grep -w cpu | awk '{print $2,$3,$4,$5,$6,$7,$8}')
  # LAST_SYS_IDLE=$(echo $LAST_CPU_INFO | awk '{print $4}')
  # LAST_TOTAL_CPU_T=$(echo $LAST_CPU_INFO | awk '{print $1+$2+$3+$4+$5+$6+$7}')
  # sleep ${TIME_INTERVAL}
  # NEXT_CPU_INFO=$(cat /proc/stat | grep -w cpu | awk '{print $2,$3,$4,$5,$6,$7,$8}')
  # NEXT_SYS_IDLE=$(echo $NEXT_CPU_INFO | awk '{print $4}')
  # NEXT_TOTAL_CPU_T=$(echo $NEXT_CPU_INFO | awk '{print $1+$2+$3+$4+$5+$6+$7}')
  #
  # #系统空闲时间
  # SYSTEM_IDLE=`echo ${NEXT_SYS_IDLE} ${LAST_SYS_IDLE} | awk '{print $1-$2}'`
  # #CPU总时间
  # TOTAL_TIME=`echo ${NEXT_TOTAL_CPU_T} ${LAST_TOTAL_CPU_T} | awk '{print $1-$2}'`
  # CPU_USAGE=`echo ${SYSTEM_IDLE} ${TOTAL_TIME} | awk '{printf "%.2f", 100-$1/$2*100}'`
  CPU_USAGE=$(top -n 1 -b | sed -n '3p' | awk '{printf "%02d%", 100 - $8}')

  echo -e "閭 ${CPU_USAGE}"

}
xsetroot -name "$(print_cpu) │ $(print_temp) │ $(print_mem) │ $(print_volume) │ $(print_battery) │ $(print_date) "
exit 0
