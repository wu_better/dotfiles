#!/bin/bash
xrandr --output eDP1 --mode 1920x1080 --left-of HDMI2
#设置键盘重复延迟 和重复频率
xset r rate 240 40
setxkbmap -option caps:escape
# setxkbmap -option caps:swapescape
#caps:escape
#xfce4-power-manager &

nm-applet &
fcitx5 -d &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
/home/wyj/.dwm/dwm-status.sh &
# feh --bg-fill ~/bak/mybackup/image/4Kalicosplay.jpg
# feh --bg-fill ~/Pictures/124712IEENK.jpg
feh --bg-fill ~/bak/mybackup/image/immg/01690.png
# picom --experimental-backend --config ~/.config/picom/picom_dwm.conf
/home/wyj/bak/mybackup/picom --experimental-backend --config ~/.config/picom/picom_an.conf
#~/.dwm/autostart_wait.sh &
