#!/bin/bash

mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.back
    echo "Server = https://mirrors.ustc.edu.cn/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist

pacstrap /mnt base base-devel linux linux-firmware alsa-utils ntfs-3g grub efibootmgr intel-ucode sudo  vim openssh man dhcpcd iwd networkmanager bash-completion xf86-video-intel alsa-firmware alsa-tools pulseaudio-alsa open-vm-tools  gtkmm xf86-video-vmware xf86-input-vmmouse 

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
echo "Arch" >> /etc/hostname
echo '127.0.0.1       localhost
::1             localhost
127.0.1.1       Arch.localdomain  Arch' >> /etc/hosts

ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc --utc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "zh_CN.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen 

echo "LANG=zh_CN.UTF-8" >> /etc/locale.conf
useradd -m -G wheel -s /bin/bash arch
echo "user password"
passwd arch
echo "root password"
passwd root




grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch
grub-mkconfig -o /boot/grub/grub.cfg

pacman -S adobe-source-han-serif-cn-fonts adobe-source-han-sans-cn-fonts wqy-zenhei  wqy-microhei noto-fonts-cjk noto-fonts-emoji noto-fonts-extra ttf-dejavu ttf-font-awesome

systemctl enable sshd NetworkManager iwd dhcpcd vmtoolsd 

sudo pacman -S fcitx5-im fcitx5-chinese-addons fcitx5-pinyin-zhwiki fcitx5-material-color
echo 'GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
XMODIFIERS=@im=fcitx
INPUT_METHOD=fcitx
SDL_IM_MODULE=fcitx
GLFW_IM_MODULE=ibus' >> /etc/environment


