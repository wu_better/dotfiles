-- 全局提示插件
if vim.g.vscode then
  return;
end
-- require("plugin-config.nvim-notify")
-- 基础配置
require("basic")
-- 快捷键映射
require("keybindings")
-- Packer插件管理
require("plugins")
-- 主题设置
require("colorscheme")
-- 自动命令
require("autocmds")

-- 插件配置
require("plugin-config.nvim-tree")
require("plugin-config.bufferline")
require("plugin-config.lualine")
-- require("plugin-config.feline")
require("plugin-config.telescope")
require("plugin-config.nvim-treesitter")
require("plugin-config.indent-blankline")
require("plugin-config.comment")
require("plugin-config.gitsigns");
require("plugin-config.alpha")
-- require("plugin-config.diffview")
require("plugin-config.which-key")
require("plugin-config.fidget")
require("plugin-config.nvim-autopairs")
require("plugin-config.symbols-outline")
require("plugin-config.project")
require("plugin-config.surround")
require("plugin-config.trouble")
require("plugin-config.nvim-ts-autotag")
require("plugin-config.toggleterm")
require("plugin-config.markdown")

-- 内置LSP
require("lsp.setup")
require("lsp.cmp")
require("lsp.ui")
require("lsp.nvim-lsp-signature")

-- 格式化
-- require("lsp.formatter")
require("lsp.null-ls")
-- DAP
-- require("dap.vimspector")
require("dap.nvim-dap")
