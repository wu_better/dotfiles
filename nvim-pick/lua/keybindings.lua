-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

local opts = { noremap = true, silent = true, }
local term_opts = { silent = true }
-- 本地变量
local keymap = vim.api.nvim_set_keymap

-- leader key 为空
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "
-- 窗口相关操作
-- ------------------------------------------------------------------
-- Better window navigation
-- -- alt + hjkl  窗口之间跳转
keymap("n", "<A-h>", "<C-w>h", opts)
keymap("n", "<A-j>", "<C-w>j", opts)
keymap("n", "<A-k>", "<C-w>k", opts)
keymap("n", "<A-l>", "<C-w>l", opts)
keymap("n", "<A-w>", "<C-w>w", opts)
keymap("n", "<A-=>", "<C-w>=", opts)
-- <leader> + hjkl 窗口之间跳转
-- keymap("n", "<leader>h", "<C-w>h", opts)
-- keymap("n", "<leader>j", "<C-w>j", opts)
-- keymap("n", "<leader>k", "<C-w>k", opts)
-- keymap("n", "<leader>l", "<C-w>l", opts)
-- -- windows 分屏快捷键
-- ------------------------------------------------------------------
-- 取消 s 默认功能
keymap("n", "s", "", opts)
keymap("n", "sd", ":TroubleToggle<CR>", opts) -- 垂直分割窗口
keymap("n", "sv", ":vsp<CR>", opts) -- 垂直分割窗口
keymap("n", "sh", ":sp<CR>", opts) -- 水平分割窗口
keymap("n", "sc", "<C-w>c", opts) -- 关闭当前
-- keymap("t", "sc", [[ <C-\><C-N><C-w>c ]], opts) -- 关闭当前
-- 关闭其他
keymap("n", "so", "<C-w>o", opts) -- close others
-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)
-- Terminal相关
keymap("n", "sg", ":terminal lazygit<CR>", opts)
keymap("n", "stt", ":sp | terminal<CR>", opts)
keymap("n", "stv", ":vsp | terminal<CR>", opts)
-- Esc 回 Normal 模式
-- keymap("t", "<Esc>", "<C-\\><C-n>", opts)
keymap("t", "<A-h>", [[ <C-\><C-N><C-w>h ]], opts)
keymap("t", "<A-j>", [[ <C-\><C-N><C-w>j ]], opts)
keymap("t", "<A-k>", [[ <C-\><C-N><C-w>k ]], opts)
keymap("t", "<A-l>", [[ <C-\><C-N><C-w>l ]], opts)
-- keymap("t", "<leader>h", [[ <C-\><C-N><C-w>h ]], opts)
-- keymap("t", "<leader>j", [[ <C-\><C-N><C-w>j ]], opts)
-- keymap("t", "<leader>k", [[ <C-\><C-N><C-w>k ]], opts)
-- keymap("t", "<leader>l", [[ <C-\><C-N><C-w>l ]], opts)
-- Visual --
-- visual模式下缩进代码
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)
-------------------------------------------------------
-- 上下移动选中文本
-- keymap("n", "<A-j>", ":m .+1<CR>==", opts)
-- keymap("n", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "J", ":move '>+1<CR>gv-gv", opts)
keymap("v", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)
-----------------------------------------------------------
-- fix :set wrap
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
keymap("n", "<C-j>", "5j", opts)
keymap("n", "<C-k>", "5k", opts)
keymap("i", "jj", "<ESC>", opts)
keymap("i", "jk", "<ESC>", opts)
keymap("i", "<Left>", "<C-G>U<Left>", opts)
keymap("i", "<Right>", "<C-G>U<Right>", opts)


-- -- $跳到行尾不带空格 (交换$ 和 g_)
-- keymap("v", "$", "g_", opts)
-- keymap("v", "g_", "$", opts)
-- keymap("n", "$", "g_", opts)
-- keymap("n", "g_", "$", opts)

-- 在visual mode 里粘贴不要复制
keymap("v", "p", '"_dP', opts)
-- magic search
keymap("n", "/", "/\\v", { noremap = true, silent = false })
keymap("v", "/", "/\\v", { noremap = true, silent = false })





keymap("n", "<leader>w", ":w<CR>", opts)
-- keymap("n", "<leader>wq", ":wqa!<CR>", opts)
keymap("n", "<leader>q", ":qa!<CR>", opts)

-- -- insert 模式下，跳到行首行尾
-- -- keymap("i", "<C-h>", "<ESC>I", opts)
-- -- keymap("i", "<C-l>", "<ESC>A", opts)
--------------------------------------------------------------------
-- 插件快捷键
local pluginKeys = {}

-- symbolsOutline
keymap("n", "<leader>o", ":SymbolsOutline<CR>", opts)
-- lazygit
keymap("n", "<leader>gg", ":LazyGit<CR>", opts)
keymap("n", "<leader>gf", ":LazyGitFilter<CR>", opts)
keymap("n", "<leader>gc", ":LazyGitFilterCurrentFile<CR>", opts)
-- treesitter 折叠
keymap("n", "zc", ":foldclose<CR>", opts)
keymap("n", "zo", ":foldopen<CR>", opts)

-- nvim-tree
keymap("n", "<A-m>", ":NvimTreeToggle<CR>", opts)
keymap("n", "<leader>m", ":NvimTreeToggle<CR>", opts)


----------------------------------------------------------
-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)
keymap("n", "<leader>bc", ":Bdelete!<CR>", opts)
-- bufferline
-- 左右Tab切换
-- keymap("n", "<S-h>", ":BufferLineCyclePrev<CR>", opts)
-- keymap("n", "<S-l>", ":BufferLineCycleNext<CR>", opts)
keymap("n", "<leader>bb", ":BufferLinePick<CR>", opts)
keymap("n", "<leader>bd", ":BufferLinePickClose<CR>", opts)
keymap("t", "<leader>bd", ":BufferLinePickClose<CR>", opts)
-- -- "moll/vim-bbye" 关闭当前 buffer
-- -- 关闭左/右侧标签页
keymap("n", "<leader>bh", ":BufferLineCloseLeft<CR>", opts)
keymap("n", "<leader>bl", ":BufferLineCloseRight<CR>", opts)
-- -- 关闭其他标签页
keymap("n", "<leader>bo", ":BufferLineCloseRight<CR>:BufferLineCloseLeft<CR>", opts)
-- -- 关闭选中标签页
--- Re-order to previous/next
keymap('n', '<A-<>', ':BufferLineMovePrev<CR>', opts)
keymap('n', '<A->>', ':BufferLineMoveNext<CR>', opts)
-- Goto buffer in position...
keymap('n', '<A-0>', ':bfirst<CR>', opts)
keymap('n', '<A-1>', ':BufferLineGoToBuffer 1<CR>', opts)
keymap('n', '<A-2>', ':BufferLineGoToBuffer 2<CR>', opts)
keymap('n', '<A-3>', ':BufferLineGoToBuffe 3<CR>', opts)
keymap('n', '<A-4>', ':BufferLineGoToBuffe 4<CR>', opts)
keymap('n', '<A-5>', ':BufferLineGoToBuffe 5<CR>', opts)
keymap('n', '<A-6>', ':BufferLineGoToBuffe 6<CR>', opts)
keymap('n', '<A-7>', ':BufferLineGoToBuffe 7<CR>', opts)
keymap('n', '<A-8>', ':BufferLineGoToBuffe 8<CR>', opts)
keymap('n', '<A-9>', ':BufferLineGoToBuffe 9<CR>', opts)

-- keymap('n', '<A-1>', ':lua require("bufferline").go_to_buffer(1, true)<cr>', opts)
-- keymap('n', '<A-2>', ':lua require("bufferline").go_to_buffer(2, true)<cr>', opts)
-- keymap('n', '<A-3>', ':lua require("bufferline").go_to_buffer(3, true)<cr>', opts)
-- keymap('n', '<A-4>', ':lua require("bufferline").go_to_buffer(4, true)<cr>', opts)
-- keymap('n', '<A-5>', ':lua require("bufferline").go_to_buffer(5, true)<cr>', opts)
-- keymap('n', '<A-6>', ':lua require("bufferline").go_to_buffer(6, true)<cr>', opts)
-- keymap('n', '<A-7>', ':lua require("bufferline").go_to_buffer(7, true)<cr>', opts)
-- keymap('n', '<A-8>', ':lua require("bufferline").go_to_buffer(8, true)<cr>', opts)
-- keymap('n', '<A-9>', ':lua require("bufferline").go_to_buffer(9, true)<cr>', opts)
-- Telescope
keymap("n", "<leader>ff", ":Telescope find_files<CR>", opts)
keymap("n", "<leader>fg", ":Telescope live_grep<CR>", opts)
keymap("n", "<leader>fb", ":Telescope buffers<CR>", opts)
keymap("n", "<leader>ft", ":Telescope treesitter<CR>", opts)
keymap("n", "<leader>fs", ":Telescope git_status<CR>", opts)
keymap("n", "<leader>fd", ":Telescope lsp_definitions<CR>", opts)
keymap("n", "<leader>fr", ":Telescope lsp_references<CR>", opts)
-- keymap("n", "<leader>fh", ":Telescope help_tags<CR>", opts)
-- 列表快捷键
pluginKeys.nvimTreeList = function(customCallback)
  return { -- 打开文件或文件夹
    { key = "<C-f>",                          cb = customCallback "launch_find_files" },
    { key = "<C-g>",                          cb = customCallback "launch_live_grep" },
    { key = { "<CR>", "o", "<2-LeftMouse>" }, action = "edit" },
    -- { key = "<C-e>",                          action = "edit_in_place" },
    -- { key = "O",                              action = "edit_no_picker" },
    -- { key = { "<C-]>", "<2-RightMouse>" },    action = "cd" },
    { key = "<C-v>",                          action = "vsplit" },
    { key = "<C-x>",                          action = "split" },
    -- { key = "<C-t>",                          action = "tabnew" },
    { key = "<",                              action = "prev_sibling" },
    { key = ">",                              action = "next_sibling" },
    { key = "P",                              action = "parent_node" },
    { key = "<BS>",                           action = "close_node" },
    { key = "<Tab>",                          action = "preview" },
    { key = "K",                              action = "first_sibling" },
    { key = "J",                              action = "last_sibling" },
    { key = "I",                              action = "toggle_git_ignored" },
    { key = "H",                              action = "toggle_dotfiles" },
    { key = "U",                              action = "toggle_custom" },
    { key = "R",                              action = "refresh" },
    { key = "a",                              action = "create" },
    { key = "d",                              action = "remove" },
    { key = "D",                              action = "trash" },
    { key = "r",                              action = "rename" },
    { key = "<C-r>",                          action = "full_rename" },
    { key = "x",                              action = "cut" },
    { key = "c",                              action = "copy" },
    { key = "p",                              action = "paste" },
    { key = "y",                              action = "copy_name" },
    { key = "Y",                              action = "copy_path" },
    { key = "gy",                             action = "copy_absolute_path" },
    -- { key = "[e",                             action = "prev_diag_item" },
    -- { key = "[c",                             action = "prev_git_item" },
    -- { key = "]e",                             action = "next_diag_item" },
    -- { key = "]c",                             action = "next_git_item"c },
    -- 进入下一级
    { key = { "]" },                          action = "cd" },
    -- 进入上一级
    { key = { "[" },                          action = "dir_up" }, -- 进入上一级
    -- { key = "-",                              action = "dir_up" }, -- 进入上一级
    { key = "s",                              action = "system_open" }, -- 文件管理器打开
    { key = "f",                              action = "live_filter" },
    { key = "F",                              action = "clear_live_filter" },
    { key = "q",                              action = "close" },
    { key = "W",                              action = "collapse_all" },
    { key = "E",                              action = "expand_all" },
    -- { key = "S",                              action = "search_node" },
    { key = ".",                              action = "run_file_command" },
    { key = "<C-k>",                          action = "toggle_file_info" },
    { key = "g?",                             action = "toggle_help" },
    -- { key = "m",                              action = "toggle_mark" },
    -- { key = "bmv",                            action = "bulk_move" },
  }
end;
-- Telescope 列表中 插入模式快捷键
pluginKeys.telescopeList = function(actions, layout)
  return {
    i = {
      -- 上下移动
      ["<C-j>"] = "move_selection_next",
      ["<C-k>"] = "move_selection_previous",
      ["<C-n>"] = "move_selection_next",
      ["<C-p>"] = "move_selection_previous",
      -- 历史记录
      ["<Down>"] = "cycle_history_next",
      ["<Up>"] = "cycle_history_prev",
      -- 关闭窗口
      -- ["<esc>"] = actions.close,
      ["<C-c>"] = actions.close --[[ "close" ]],
      -- 预览窗口上下滚动
      ["<C-u>"] = "preview_scrolling_up",
      ["<C-d>"] = "preview_scrolling_down",
      ["<C-v>"] = layout.toggle_preview,
    },
    n = {
      ["<C-c>"] = actions.close --[[ "close" ]],
      ['<C-v'] = layout.toggle_preview,
    }
  }
end;

-- 代码注释插件
-- see ./lua/plugin-config/comment.lua
pluginKeys.comment = {
  -- Normal 模式快捷键
  toggler = {
    line = "gcc", -- 行注释
    block = "gbc", -- 块注释
  },
  -- Visual 模式
  opleader = {
    line = "gc",
    bock = "gb",
  },
}
-- ctrl + /
keymap("n", "<C-_>", "gcc", { noremap = false })
keymap("v", "<C-_>", "gcc", { noremap = false })

-- lsp 回调函数快捷键设置
pluginKeys.mapLSP = function(mapbuf)
  -- hover
  mapbuf("n", "gh", "<cmd>Lspsaga hover_doc<CR>", opts)
  -- mapbuf("n", "gh", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)

  -- rename
  mapbuf("n", "<leader>cr", "<cmd>Lspsaga rename<CR>", opts)
  -- mapbuf("n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)

  -- code action
  mapbuf("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts)
  -- mapbuf("n", "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)

  mapbuf('n', '<leader>ci', '<cmd>Lspsaga implement<CR>', opts)
  mapbuf('n', '<leader>cp', '<cmd>Lspsaga peek_definition<CR>', opts)
  mapbuf('n', '<leader>co', '<cmd>Lspsaga outline<CR>', opts)
  mapbuf('n', '<leader>ct', '<cmd>Lspsaga term_toggle<CR>', opts)

  -- mapbuf("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
  mapbuf("n", "gd", "<cmd>lua require'telescope.builtin'.lsp_definitions({ initial_mode = 'normal', })<CR>", opts)

  -- Lspsaga 替换 gr
  mapbuf("n", "gr", "<cmd>Lspsaga lsp_finder<CR>", opts)
  -- mapbuf("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  --[[
  Lspsaga 替换 gp, gj, gk
  mapbuf("n", "gp", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
  mapbuf("n", "gj", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts)
  mapbuf("n", "gk", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts)
  --]]
  -- diagnostic
  mapbuf("n", "<leader>sd", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)
  mapbuf("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<cr>", opts)
  mapbuf("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<cr>", opts)
  mapbuf("n", "sf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", opts)
  mapbuf("v", "sf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", opts)
  -- 未用
  -- mapbuf("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  -- mapbuf("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
  -- mapbuf('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
  -- mapbuf("n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
  -- mapbuf('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  -- mapbuf('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  -- mapbuf('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  -- mapbuf('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
end

-- typescript 快捷键
pluginKeys.mapTsLSP = function(mapbuf)
  mapbuf("n", "gs", ":TSLspOrganize<CR>", opts)
  mapbuf("n", "gR", ":TSLspRenameFile<CR>", opts)
  mapbuf("n", "gi", ":TSLspImportAll<CR>", opts)
end

-- nvim-dap
pluginKeys.mapDAP = function()
  keymap(
    "n",
    "<leader>de",
    ":lua require'dap'.close()<CR>"
    .. ":lua require'dap'.terminate()<CR>"
    .. ":lua require'dap.repl'.close()<CR>"
    .. ":lua require'dapui'.close()<CR>"
    .. ":lua require('dap').clear_breakpoints()<CR>"
    .. "<C-w>o<CR>",
    opts
  )
  -- 继续
  keymap("n", "<leader>dc", ":lua require'dap'.continue()<CR>", opts)
  -- 设置断点
  keymap("n", "<leader>dt", ":lua require('dap').toggle_breakpoint()<CR>", opts)
  keymap("n", "<leader>dT", ":lua require('dap').clear_breakpoints()<CR>", opts)
  --  stepOver, stepOut, stepInto
  keymap("n", "<leader>dj", ":lua require'dap'.step_over()<CR>", opts)
  keymap("n", "<leader>dk", ":lua require'dap'.step_out()<CR>", opts)
  keymap("n", "<leader>dl", ":lua require'dap'.step_into()<CR>", opts)
  -- 弹窗
  keymap("n", "<leader>dh", ":lua require'dapui'.eval()<CR>", opts)
end

-- nvim-cmp 自动补全
pluginKeys.cmp = function(cmp)
  local feedkey = function(key, mode)
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
  end
  local has_words_before = function()
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
  end
  local luasnip = require("luasnip")

  return {
    -- 上一个
    ["<C-k>"] = cmp.mapping.select_prev_item(),
    -- 下一个
    ["<C-j>"] = cmp.mapping.select_next_item(),
    -- 出现补全
    ["<A-o>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
    -- 取消
    ["<A-c>"] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    -- 确认
    -- Accept currently selected item. If none selected, `select` first item.
    -- Set `select` to `false` to only confirm explicitly selected items.
    ["<CR>"] = cmp.mapping.confirm({
      select = true,
      behavior = cmp.ConfirmBehavior.Replace,
    }),
    -- ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    -- 如果窗口内容太多，可以滚动
    ["<C-u>"] = cmp.mapping(cmp.mapping.scroll_docs( -3), { "i", "c" }),
    ["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(3), { "i", "c" }),
    -- snippets 站位符跳转
    -- ["<A-l>"] = cmp.mapping(function(_)
    --   if vim.fn["vsnip#available"](1) == 1 then
    --     feedkey("<Plug>(vsnip-expand-or-jump)", "")
    --   end
    -- end, { "i", "s" }),
    -- ["<A-h>"] = cmp.mapping(function()
    --   if vim.fn["vsnip#jumpable"]( -1) == 1 then
    --     feedkey("<Plug>(vsnip-jump-prev)", "")
    --   end
    -- end, { "i", "s" }),
    -- -- super Tab
    -- ["<Tab>"] = cmp.mapping(function(fallback)
    --   if cmp.visible() then
    --     cmp.select_next_item()
    --   elseif vim.fn["vsnip#available"](1) == 1 then
    --     feedkey("<Plug>(vsnip-expand-or-jump)", "")
    --   elseif has_words_before() then
    --     cmp.complete()
    --   else
    --     fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
    --   end
    -- end, { "i", "s" }),
    -- ["<S-Tab>"] = cmp.mapping(function()
    --   if cmp.visible() then
    --     cmp.select_prev_item()
    --   elseif vim.fn["vsnip#jumpable"]( -1) == 1 then
    --     feedkey("<Plug>(vsnip-jump-prev)", "")
    --   end
    -- end, { "i", "s" }),
    -- end of super Tab
    ["<A-l>"] = cmp.mapping(function(fallback)
      if luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<A-h>"] = cmp.mapping(function(fallback)
      if luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      -- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable() 
      -- they way you will only jump inside the snippet region
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),

    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
  }
end

-- -- 自定义 toggleterm 3个不同类型的命令行窗口
-- -- <leader>ta 浮动
-- -- <leader>tb 右侧
-- -- <leader>tc 下方
-- -- 特殊lazygit 窗口，需要安装lazygit
-- -- <leader>tg lazygit
-- -- <leader>tr ranger
pluginKeys.mapToggleTerm = function(toggleterm)
  vim.keymap.set({ "n", "t" }, "<leader>tt", "<cmd>ToggleTermToggle<CR>", opts)
  vim.keymap.set({ "n", "t" }, "<leader>ta", toggleterm.toggleA)
  vim.keymap.set({ "n", "t" }, "<leader>tb", toggleterm.toggleB)
  vim.keymap.set({ "n", "t" }, "<leader>tc", toggleterm.toggleC)
  vim.keymap.set({ "n", "t" }, "<leader>tg", toggleterm.toggleG)
  vim.keymap.set({ "n", "t" }, "<leader>tr", toggleterm.toggleR)
end

-- gitsigns
pluginKeys.gitsigns_on_attach = function(bufnr)
  local gs = package.loaded.gitsigns

  local function map(mode, l, r, opt)
    opt = opt or {}
    opt.buffer = bufnr
    vim.keymap.set(mode, l, r, opt)
  end

  -- Navigation
  map("n", "<leader>gj", function()
    if vim.wo.diff then
      return "]c"
    end
    vim.schedule(function()
      gs.next_hunk()
    end)
    return "<Ignore>"
  end, { expr = true })

  map("n", "<leader>gk", function()
    if vim.wo.diff then
      return "[c"
    end
    vim.schedule(function()
      gs.prev_hunk()
    end)
    return "<Ignore>"
  end, { expr = true })

  map({ "n", "v" }, "<leader>gs", ":Gitsigns stage_hunk<CR>")
  map("n", "<leader>gS", gs.stage_buffer)
  map("n", "<leader>gu", gs.undo_stage_hunk)
  map({ "n", "v" }, "<leader>gr", ":Gitsigns reset_hunk<CR>")
  map("n", "<leader>gR", gs.reset_buffer)
  map("n", "<leader>gp", gs.preview_hunk)
  map("n", "<leader>gb", function()
    gs.blame_line({ full = true })
  end)
  map("n", "<leader>gd", gs.diffthis)
  map("n", "<leader>gD", function()
    gs.diffthis("~")
  end)
  -- toggle
  map("n", "<leader>gtd", gs.toggle_deleted)
  map("n", "<leader>gtb", gs.toggle_current_line_blame)
  -- Text object
  map({ "o", "x" }, "ig", ":<C-U>Gitsigns select_hunk<CR>")
end

return pluginKeys
