vim.g.mkdp_browser = 'wyeb'
vim.g.mkdp_markdown_css = '/home/chenyc/.config/nvim/lua/colorscheme/markdown.css'
vim.g.mkdp_page_title = '${name}'
vim.g.mkdp_preview_options = { hide_yaml_meta = 1, disable_filename = 1 }
vim.g.mkdp_theme = 'light'
vim.g.vmt_fence_text = 'markdown-toc'
