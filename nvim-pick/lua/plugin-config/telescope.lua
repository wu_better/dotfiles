local status, telescope = pcall(require, "telescope")
if not status then
  vim.notify("没有找到 telescope")
  return
end

local actions = require("telescope.actions")
local layout = require("telescope.actions.layout")
telescope.setup({
  defaults = {
    prompt_prefix = " ",
    selection_caret = " ",
    -- 打开弹窗后进入的初始模式，默认为 insert，也可以是 normal
    initial_mode = "insert",
    -- vertical , center , cursor
    layout_strategy = "horizontal",
    -- 窗口内快捷键
    mappings = require("keybindings").telescopeList(actions, layout),
  },
  pickers = {
    find_files = {
      -- theme = "dropdown", -- 可选参数： dropdown, cursor, ivy
      -- theme = "dropdown",
      -- previewer = false,
      -- find_command = { "find", "-type", "f" },
      find_command = { "fd", "-I", "-E", "node_modules" }, -- "-H" search hidden files, "-I" do not respect to gitignore
    },
  },
  extensions = {
    ["ui-select"] = {
      require("telescope.themes").get_dropdown({
        -- even more opts
      }),
    },
  },
})

-- pcall(telescope.load_extension, "env")
-- To get ui-select loaded and working with telescope, you need to call
-- load_extension, somewhere after setup function:
pcall(telescope.load_extension, "ui-select")
