-- require("colorscheme.tokyonight")
require("colorscheme.onedark")
-- require("colorscheme.onedarkpro")

-- zephyr
-- vim.o.background = "dark"
-- local colorscheme = "zephyr"
-- local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
-- if not status_ok then
--   vim.notify("colorscheme: " .. colorscheme .. " 没有找到！")
--   return
-- end
vim.cmd('highlight Pmenu ctermfg=0 ctermbg=13 guifg=#a0a8b7 guibg=None');
vim.cmd('highlight NormalFloat guifg=#a0a8b7 guibg=None');
vim.cmd('highlight FloatBorder guifg=#98C379 guibg=None')

vim.cmd('highlight CodeActionNumber guifg=#c678dd guibg=None')
-- vim.cmd('highlight DiagnosticVirtualTextError guifg=#8b3434 guibg=None');
-- vim.cmd('highlight DiagnosticVirtualTextWarn guifg=#835d1a guibg=None');
-- vim.cmd('highlight DiagnosticVirtualTextInfo guifg=#266269 guibg=None');
-- vim.cmd('highlight DiagnosticVirtualTextHint guifg=#7e3992 guibg=None');
