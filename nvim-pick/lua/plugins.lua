-- 自动安装 Packer.nvim
-- 插件安装目录
-- ~/.local/share/nvim/site/pack/packer/
local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
local paccker_bootstrap
if fn.empty(fn.glob(install_path)) > 0 then
  vim.notify("正在安装Pakcer.nvim，请稍后...")
  paccker_bootstrap = fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })

  -- https://github.com/wbthomason/packer.nvim/issues/750
  local rtp_addition = vim.fn.stdpath("data") .. "/site/pack/*/start/*"
  if not string.find(vim.o.runtimepath, rtp_addition) then
    vim.o.runtimepath = rtp_addition .. "," .. vim.o.runtimepath
  end
  vim.notify("Pakcer.nvim 安装完毕")
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("没有安装 packer.nvim")
  return
end

packer.startup({
  function(use)
    -- Packer 可以升级自己
    use("wbthomason/packer.nvim")
    -------------------------- plugins -------------------------------------------
    -- nvim-tree
    use({
      "nvim-tree/nvim-tree.lua",
      requires = "nvim-tree/nvim-web-devicons",
    })
    -- trouble诊断
    use({
      "folke/trouble.nvim",
      requires = "nvim-tree/nvim-web-devicons",
    })
    -- bufferline
    use({
      "akinsho/bufferline.nvim",
      tag = "v3.*",
      requires = { "nvim-tree/nvim-web-devicons", "famiu/bufdelete.nvim" },
    })
    -- lualine
    use({
      "nvim-lualine/lualine.nvim",
      requires = { "nvim-tree/nvim-web-devicons" },
    })
    -- telescope
    use({
      "nvim-telescope/telescope.nvim",
      requires = { "nvim-lua/plenary.nvim" },
    })
    -- telescope extensions
    -- use("LinArcX/telescope-env.nvim")
    use("nvim-telescope/telescope-ui-select.nvim")
    -- -- dashboard-nvim
    -- use("glepnir/dashboard-nvim")
    -- alpha
    use({
      'goolord/alpha-nvim',
      requires = { 'nvim-tree/nvim-web-devicons' },
    })
    -- project
    use("ahmedkhalf/project.nvim")
    -- treesitter
    use({
      "nvim-treesitter/nvim-treesitter",
      run = ":TSUpdate",
    })
    -- nvim-treesitter 增强
    use("nvim-treesitter/nvim-treesitter-refactor")


    use("p00f/nvim-ts-rainbow")
    -- outline
    use({
      "simrat39/symbols-outline.nvim",
      -- commit = "6e7af4cf7def9365ec00068da8545ed85717ab48"
    })
    -- -- indent-blankline
    use("lukas-reineke/indent-blankline.nvim")
    --------------------- LSP --------------------
    -- -- Lspconfig
    use {
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "neovim/nvim-lspconfig",
    }
    -- -- 补全引擎
    use({
      "hrsh7th/nvim-cmp",
      requires = {
        "hrsh7th/cmp-nvim-lsp", -- { name = nvim_lsp }
        "hrsh7th/cmp-buffer", -- { name = 'buffer' },
        "hrsh7th/cmp-path", -- { name = 'path' }
        "hrsh7th/cmp-cmdline", -- { name = 'cmdline' }
        'L3MON4D3/LuaSnip',
        'saadparwaiz1/cmp_luasnip',
        --片段补全
        -- "hrsh7th/cmp-vsnip", -- {name = vsnip} 片段补全
        -- -- Snippet 引擎
        -- "hrsh7th/vim-vsnip",
        -- 常见编程语言代码段
        "rafamadriz/friendly-snippets",

      }
    })
    -- -- 补全源
    -- use("hrsh7th/cmp-nvim-lsp-signature-help") -- { name = 'nvim_lsp_signature_help' }
    use("ray-x/lsp_signature.nvim")


    -- -- UI 增强
    use("onsails/lspkind-nvim")
    -- use("tami5/lspsaga.nvim")
    use({
      "glepnir/lspsaga.nvim",
      branch = "main",
      -- commit = "133bf4b06f109da0cae9ab61a6dd8e29e62c90d3",
      commit = "438b54cba00fca27d280ae4d9242615282045bcb",
      config = function()
        require("lsp.lspsaga")
      end,
      requires = { { "nvim-tree/nvim-web-devicons" }, { "nvim-treesitter/nvim-treesitter" } }
    })
    -- -- 代码格式化
    -- use("mhartington/formatter.nvim")
    use({ "jose-elias-alvarez/null-ls.nvim", requires = "nvim-lua/plenary.nvim" })
    -- TypeScript 增强
    -- use({ "jose-elias-alvarez/nvim-lsp-ts-utils", requires = "nvim-lua/plenary.nvim" })
    use("jose-elias-alvarez/typescript.nvim")
    -- Lua 增强
    use("folke/neodev.nvim")
    -- JSON 增强
    use("b0o/schemastore.nvim")
    -- Rust 增强
    use("simrat39/rust-tools.nvim")
    --------------------- colorschemes --------------------
    -- -- tokyonight
    use("folke/tokyonight.nvim")
    -- -- OceanicNext
    -- use("mhartington/oceanic-next")
    -- -- gruvbox
    -- use({
    --   "ellisonleao/gruvbox.nvim",
    --   requires = { "rktjmp/lush.nvim" },
    -- })
    -- use('shaunsingh/moonlight.nvim')
    -- zephyr
    use("glepnir/zephyr-nvim")
    -- -- nord
    -- use("shaunsingh/nord.nvim")
    -- onedark
    -- use("ful1e5/onedark.nvim")
    -- use("olimorris/onedarkpro.nvim")
    use("navarasu/onedark.nvim")
    -- -- nightfox
    -- use("EdenEast/nightfox.nvim")

    -- -------------------------------------------------------
    use({ "akinsho/toggleterm.nvim" })
    -- surround
    use("ur4ltz/surround.nvim")
    -- -- Comment
    use("numToStr/Comment.nvim")
    -- nvim-autopairs
    use("windwp/nvim-autopairs")
    use("windwp/nvim-ts-autotag")
    -- -- git
    use("kdheepak/lazygit.nvim")
    use({ "lewis6991/gitsigns.nvim" })
    use({ 'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim' })
    -- -- vimspector
    -- use("puremourning/vimspector")
    -- ----------------------------------------------
    use("mfussenegger/nvim-dap")
    use("theHamsta/nvim-dap-virtual-text")
    use("rcarriga/nvim-dap-ui")
    -- Packer
    use({
      "folke/noice.nvim",
      config = function()
        require("plugin-config.nvim-notify")
        -- require("noice")
      end,
      requires = {
        "MunifTanjim/nui.nvim",
        "rcarriga/nvim-notify",
      }
    })
    use("j-hui/fidget.nvim")
    use("folke/which-key.nvim")
    use({
      "iamcco/markdown-preview.nvim",
      run = function() vim.fn["mkdp#util#install"]() end,
    })
    use({
      "karb94/neoscroll.nvim",
      config = function()
        require("neoscroll").setup({})
      end
    })
    use({
      'mg979/vim-visual-multi',
      branch = 'master'
    })
    if paccker_bootstrap then
      packer.sync()
    end
  end
})
