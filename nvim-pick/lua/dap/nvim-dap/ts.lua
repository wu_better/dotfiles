local M = {}

function M.setup()
  local dap = require("dap")
  dap.adapters.node2 = {
    type = 'executable',
    command = 'node',
    args = { '/home/wyj/.local/share/nvim/dap_servers/vscode-node-debug2/out/src/nodeDebug.js' },
  }
  dap.configurations.javascript = {
    {
      name = 'Launch',
      type = 'node2',
      request = 'launch',
      program = '${file}',
      -- program = function ()
      --   return coroutine.create(function(dap_run_co)
      --     vim.ui.input({prompt = "Enter value: "}, function(choice)
      --       coroutine.resume(dap_run_co, choice)
      --     end)
      --   end)
      -- end,
      cwd = vim.fn.getcwd(),
      sourceMaps = true,
      protocol = 'inspector',
      console = 'integratedTerminal',
    },
    {
      -- For this to work you need to make sure the node process is started with the `--inspect` flag.
      name = 'Attach to process',
      type = 'node2',
      request = 'attach',
      processId = require 'dap.utils'.pick_process,
    },
  }

  -- dap.configurations.javascript = {
  --   {
  --     name = "Launch",
  --     type = "node2",
  --     request = "attach",
  --     program = '${file}',
  --     cwd = vim.fn.getcwd(),
  --     sourceMaps = true,
  --     protocol = 'inspector',
  --     console = 'integratedTerminal',
  --   },
  --   {
  --     -- For this to work you need to make sure the node process is started with the `--inspect` flag.
  --     name = 'Attach to process',
  --     type = 'node2',
  --     request = 'attach',
  --     processId = require'dap.utils'.pick_process,
  --   },
  -- }
end

-- function M.open()
--   require("osv").run_this()
-- end
return M
