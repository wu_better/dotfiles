-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
-- local Util = require("lazyvim.util")
-- local map = LazyVim.safe_keymap_set
local map = vim.keymap.set
local opts = function(desc)
  return { noremap = true, silent = true, desc = desc }
end
map("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
map("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
map("v", "p", '"_dP', opts(""))
map({ "n", "x" }, "gw", "*N", { desc = "Search word under cursor" })
map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")
-- map("v", "J", ":m '>+1<CR>gv=gv")
-- map("v", "K", ":m '>-2<CR>gv=gv")

-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
map("n", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("x", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("o", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("n", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("x", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("o", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("n", "<leader>j", "<cmd>w<cr><esc>", { desc = "Save file" })
map("n", "<leader>wo", "<C-w>o", { desc = "close other window" })
