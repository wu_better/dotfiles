local temp_path = "/tmp/togterm"
local Terminal = require("toggleterm.terminal").Terminal
local lazygit = Terminal:new({
  cmd = "lazygit",
  dir = "git_dir",
  direction = "float",
  float_opts = {
    border = "single", --'single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
    width = function(term)
      return math.floor(vim.o.columns * 0.95)
    end,
    height = function(term)
      return math.floor(vim.o.lines * 0.95)
    end,
  },
  on_open = function(term)
    vim.cmd("startinsert!")
    -- q / <leader>tg 关闭 terminal
    vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(term.bufnr, "n", "<leader>tg", "<cmd>close<CR>", { noremap = true, silent = true })
  end,
  on_close = function(_) end,
})
local ranger = Terminal:new({
  cmd = "ranger --choosefiles='" .. temp_path .. "'",
  dir = "git_dir",
  direction = "float",
  float_opts = {
    border = "single",
    width = function(term)
      return math.floor(vim.o.columns * 0.95)
    end,
    height = function(term)
      return math.floor(vim.o.lines * 0.95)
    end,
  },
  on_open = function(term)
    vim.cmd("startinsert!")
    -- q / <leader>tg 关闭 terminal
    vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(term.bufnr, "n", "<leader>tr", "<cmd>close<CR>", { noremap = true, silent = true })
  end,
  on_close = function(_)
    vim.notify("ranger close")
    print("range close")
    local file = io.open(temp_path, "r")
    if file ~= nil then
      local file_path = file:read("*a")
      file:close()
      vim.cmd("silent! vsplit | e " .. file_path)
      os.remove(temp_path)
    end
  end,
})

local ta = Terminal:new({
  direction = "float",
  close_on_exit = true,
})

local tb = Terminal:new({
  direction = "vertical",
  close_on_exit = true,
})

local tc = Terminal:new({
  direction = "horizontal",
  close_on_exit = true,
})

local M = {}

M.toggleA = function()
  if ta:is_open() then
    ta:close()
    return
  end
  tb:close()
  tc:close()
  ta:open()
end

M.toggleB = function()
  if tb:is_open() then
    tb:close()
    return
  end
  ta:close()
  tc:close()
  tb:open()
end

M.toggleC = function()
  if tc:is_open() then
    tc:close()
    return
  end
  ta:close()
  tb:close()
  tc:open()
end

M.toggleR = function()
  ranger:toggle()
end
M.toggleG = function()
  lazygit:toggle()
end
return M
