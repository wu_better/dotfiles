local map = vim.keymap.set
local opts = function(desc)
  return { noremap = true, silent = true, desc = desc }
end
vim.g.mapleader = " "
vim.g.maplocalleader = " "

map("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
map("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
map("v", "p", '"_dP', opts(""))
map({ "n", "x" }, "gw", "*N", { desc = "Search word under cursor" })
map("v", "<", "<gv", opts(""))
map("v", ">", ">gv", opts(""))

-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
-- map("n", "n", "'Nn'[v:searchforward]")
-- map("x", "n", "'Nn'[v:searchforward]")
-- map("o", "n", "'Nn'[v:searchforward]")
-- map("n", "N", "'nN'[v:searchforward]")
-- map("x", "N", "'nN'[v:searchforward]")
-- map("o", "N", "'nN'[v:searchforward]")
map("n", "gcc", "<cmd>call VSCodeNotify('editor.action.commentLine')<CR>")
map("v", "gc", "<cmd>call VSCodeNotify('editor.action.commentLine')<CR>")
map("v", "gb", "<cmd>call VSCodeNotify('editor.action.blockComment')<CR>")
map("n", "<A-j>", "<cmd>call VSCodeNotify('editor.action.moveLinesDownAction')<CR>")
map("n", "<A-k>", "<cmd>call VSCodeNotify('editor.action.moveLinesUpAction')<CR>")
-- 切换当下折叠
map("n", "za", "<cmd>call VSCodeNotify('editor.toggleFold')<CR>")
map("n", "u", "<cmd>call VSCodeNotify('undo')<CR>")

map("n", "<leader>sf", "<cmd>call VSCodeNotify('editor.action.formatDocument')<CR>")
map("v", "<leader>sf", "<cmd>call VSCodeNotify('editor.action.formatSelection')<CR>")
map("n", "<C-h>", "<cmd>call VSCodeNotify('workbench.action.focusLeftGroup')<CR>")
map("n", "<C-l>", "<cmd>call VSCodeNotify('workbench.action.focusRightGroup')<CR>")
map("n", "<C-j>", "<cmd>call VSCodeNotify('workbench.action.focusBelowGroup')<CR>")
map("n", "<C-k>", "<cmd>call VSCodeNotify('workbench.action.focusAboveGroup')<CR>")
map("n", "<leader>wd", "<cmd>call VSCodeNotify('workbench.action.closeActiveEditor')<CR>")
map("n", "<leader>bd", "<cmd>call VSCodeNotify('workbench.action.closeActiveEditor')<CR>")

map("n", "<S-l>", "<cmd>call VSCodeNotify('workbench.action.nextEditor')<CR>")
map("n", "<S-h>", "<cmd>call VSCodeNotify('workbench.action.previousEditor')<CR>")

map("n", "<S-k>", "<cmd>call VSCodeNotify('editor.action.showHover')<CR>")
map("n", "<leader>ca", "<cmd>call VSCodeNotify('editor.action.codeAction')<CR>")

map("n", "]g", "<cmd>call VSCodeNotify('workbench.action.editor.nextChange')<CR>")
map("n", "[g", "<cmd>call VSCodeNotify('workbench.action.editor.previousChange')<CR>")
