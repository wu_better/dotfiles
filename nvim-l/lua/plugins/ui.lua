---@diagnostic disable: no-unknown
return {
  {
    "nvim-lualine/lualine.nvim",
    event = "VeryLazy",
    opts = function(_, opts)
      -- opts.options.section_separators = { left = "", right = "" }
      -- opts.options.component_separators = { left = "", right = "" }
      -- opts.options.component_separators = { left = "", right = "" }
      -- opts.options.section_separators = { left = "", right = "" }
      opts.options.component_separators = { left = "", right = "" }
      opts.options.section_separators = { left = "", right = "" }
      opts.options.theme = {
        normal = {
          a = { bg = "transparent" },
          b = { bg = "transparent" },
          c = { bg = "transparent" },
          x = { bg = "transparent" },
          y = { bg = "transparent" },
          z = { bg = "transparent" },
        },
        insert = {
          a = { bg = "transparent" },
          b = { bg = "transparent" },
          c = { bg = "transparent" },
        },
        visual = {
          a = { bg = "transparent" },
          b = { bg = "transparent" },
          c = { bg = "transparent" },
        },
        replace = {
          a = { bg = "transparent" },
          b = { bg = "transparent" },
          c = { bg = "transparent" },
        },
      }

      table.insert(opts.sections.lualine_x, { "111😊", color = { bg = "transparent", fg = "#ffffff", gui = "bold" } })

      table.insert(opts.sections.lualine_x, {
        function()
          local msg = ""
          local buf_ft = vim.api.nvim_buf_get_option(0, "filetype")
          local clients = vim.lsp.get_active_clients()
          if next(clients) == nil then
            return msg
          end
          local client_name = ""
          for _, client in ipairs(clients) do
            local filetypes = client.config.filetypes
            if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
              client_name = client_name .. "@" .. client.name
            end
          end
          return client_name or msg
        end,
        icon = " ",
        color = { fg = "#008080", gui = "bold" },
      })
    end,
  },
  {
    "akinsho/bufferline.nvim",
    keys = {
      { "<leader>bs", "<Cmd>BufferLinePick<CR>", desc = "Select buffer" },
      -- { "<leader>bP", "<Cmd>BufferLineGroupClose ungrouped<CR>", desc = "Delete Non-Pinned Buffers" },
      -- { "<leader>bo", "<Cmd>BufferLineCloseOthers<CR>", desc = "Delete Other Buffers" },
      -- { "<leader>br", "<Cmd>BufferLineCloseRight<CR>", desc = "Delete Buffers to the Right" },
      -- { "<leader>bl", "<Cmd>BufferLineCloseLeft<CR>", desc = "Delete Buffers to the Left" },
      -- { "<S-h>", "<cmd>BufferLineCyclePrev<cr>", desc = "Prev Buffer" },
      -- { "<S-l>", "<cmd>BufferLineCycleNext<cr>", desc = "Next Buffer" },
      -- { "[b", "<cmd>BufferLineCyclePrev<cr>", desc = "Prev Buffer" },
      -- { "]b", "<cmd>BufferLineCycleNext<cr>", desc = "Next Buffer" },
      { "<leader>b1", "<cmd>lua require('bufferline').go_to(1, true)<cr>", desc = "go to 1 buffer" },
      { "<leader>b2", "<cmd>lua require('bufferline').go_to(2, true)<cr>", desc = "" },
      { "<leader>b3", "<cmd>lua require('bufferline').go_to(3, true)<cr>", desc = "" },
      { "<leader>b4", "<cmd>lua require('bufferline').go_to(4, true)<cr>", desc = "" },
      { "<leader>b5", "<cmd>lua require('bufferline').go_to(5, true)<cr>", desc = "" },
      { "<leader>b6", "<cmd>lua require('bufferline').go_to(6, true)<cr>", desc = "" },
      { "<leader>b7", "<cmd>lua require('bufferline').go_to(7, true)<cr>", desc = "" },
      { "<leader>b8", "<cmd>lua require('bufferline').go_to(8, true)<cr>", desc = "" },
      { "<leader>b9", "<cmd>lua require('bufferline').go_to(9, true)<cr>", desc = "" },
      { "<leader>b$", "<cmd>lua require('bufferline').go_to(-1, true)<cr>", desc = "" },
      { "<leader>bm1", "<cmd>lua require('bufferline').move_to(1)<cr>", desc = "go to 1 buffer" },
      { "<leader>bm2", "<cmd>lua require('bufferline').move_to(2)<cr>", desc = "" },
      { "<leader>bm3", "<cmd>lua require('bufferline').move_to(3)<cr>", desc = "" },
      { "<leader>bm4", "<cmd>lua require('bufferline').move_to(4)<cr>", desc = "" },
      { "<leader>bm5", "<cmd>lua require('bufferline').move_to(5)<cr>", desc = "" },
      { "<leader>bm6", "<cmd>lua require('bufferline').move_to(6)<cr>", desc = "" },
      { "<leader>bm7", "<cmd>lua require('bufferline').move_to(7)<cr>", desc = "" },
      { "<leader>bm8", "<cmd>lua require('bufferline').move_to(8)<cr>", desc = "" },
      { "<leader>bm9", "<cmd>lua require('bufferline').move_to(9)<cr>", desc = "" },
      { "<leader>bm$", "<cmd>lua require('bufferline').move_to(-1)<cr>", desc = "" },
    },
    opts = {
      options = {
        numbers = "ordinal",
        indicator = {
          icon = "│", -- this should be omitted if indicator style is not 'icon'
        },
        diagnostics = false,
      },
    },
  },
  {
    "folke/noice.nvim",
    event = "VeryLazy",
    opts = {
      presets = {
        lsp_doc_border = true,
      },
      routes = {
        {
          filter = {
            event = "msg_show",
            kind = "",
            find = "written",
          },
          opts = { skip = true },
        },
      },
    },
  },
}
