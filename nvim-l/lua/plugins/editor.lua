---@diagnostic disable: no-unknown
return {
  {
    "nvim-neo-tree/neo-tree.nvim",
    opts = {
      window = {
        mappings = {
          ["T"] = {
            function(state)
              local node = state.tree:get_node()
              -- while node and node.type ~= "directory" do
              --   local parent_id = node:get_parent_id()
              --   node = parent_id and state.tree:get_node(parent_id) or nil
              -- end
              --
              if not node or not node.path then
                return
              end
              os.execute("thunar " .. node.path)
            end,
            desc = "open with System File Manager",
          },
          ["f"] = {
            function(state)
              local node = state.tree:get_node()
              while node and node.type ~= "directory" do
                local parent_id = node:get_parent_id()
                node = parent_id and state.tree:get_node(parent_id) or nil
              end

              if not node or not node.path then
                return
              end
              vim.cmd("Neotree close")
              require("spectre").open({
                is_insert_mode = true,
                -- the directory where the search tool will be started in
                cwd = node.path,
                -- search_text = "test",
                -- replace_text = "test",
                -- the pattern of files to consider for searching
                -- path = "lua/**/*.lua",
                -- the directories or files to search in
                -- search_paths = { "lua/", "plugin/" },
                is_close = true, -- close an exists instance of spectre and open new
              })
            end,
            desc = "Search in current dir",
          },
        },
      },
    },
  },
  {
    "nvim-pack/nvim-spectre",
    keys = {
      {
        "<leader>se",
        function()
          require("spectre").open_file_search({ select_word = true })
        end,
        desc = "Search on current file",
      },
    },
  },
  {
    "nvim-telescope/telescope.nvim",
    opts = function(_, opts)
      local layout = require("telescope.actions.layout")
      opts.defaults.mappings = {
        i = {
          ["<C-v>"] = layout.toggle_preview,
          ["<Down>"] = require("telescope.actions").cycle_history_next,
          ["<Up>"] = require("telescope.actions").cycle_history_prev,
        },
        n = {
          ["<C-v>"] = layout.toggle_preview,
        },
      }
    end,
  },
  {
    "lewis6991/gitsigns.nvim",
    opts = {
      signs = {
        add = { text = "│" },
        change = { text = "│" },
        -- delete = { text = "_" },
        -- topdelete = { text = "‾" },
        delete = { text = "" },
        topdelete = { text = "" },
        changedelete = { text = "~" },
        untracked = { text = "┆" },
      },
    },
  },
  {
    "folke/flash.nvim",
    event = "VeryLazy",
    ---@type Flash.Config
    opts = {

      modes = {
        -- options used when flash is activated through
        -- a regular search with `/` or `?`
        search = {
          enabled = false,
        },
      },
    },
  },
  -- {
  --   "NeogitOrg/neogit",
  --   dependencies = {
  --     "nvim-lua/plenary.nvim", -- required
  --     "sindrets/diffview.nvim", -- optional - Diff integration
  --
  --     -- Only one of these is needed, not both.
  --     "nvim-telescope/telescope.nvim", -- optional
  --     "ibhagwan/fzf-lua", -- optional
  --   },
  --   config = true,
  -- },
}
