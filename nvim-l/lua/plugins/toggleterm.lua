---@diagnostic disable: unused-local
return {
  {
    "akinsho/toggleterm.nvim",
    -- event = "VeryLazy",
    lazy = true,
    keys = {
      -- add a keymap to browse plugin files
      -- stylua: ignore
      {
        "<leader>tt",
        mode ={"n","t"},
        -- "<cmd>ToggleTermToggle<CR>",
        desc = "ToggleTermToggle",
      },
      {
        "<leader>ta",
        mode = { "n", "t" },
        -- function()
        --   require("util.t").toggleA()
        -- end,
        desc = "open float term",
      },
      {
        "<leader>tb",
        mode = { "n", "t" },
        -- function()
        --   require("util.t").toggleB()
        -- end,
        desc = "open right term",
      },
      {
        "<leader>tc",
        mode = { "n", "t" },
        -- function()
        --   require("util.t").toggleC()
        -- end,
        desc = "open bottom term",
      },
      {
        "<leader>tg",
        mode = { "n", "t" },
        -- function()
        --   require("util.t").toggleG()
        -- end,
        desc = "open lazygit",
      },
      {
        "<leader>gg",
        mode = { "n", "t" },
        -- function()
        --   require("util.t").toggleG()
        -- end,
        desc = "open lazygit",
      },
      {
        "<leader>tr",
        mode = { "n", "t" },
        -- function()
        --   require("util.t").toggleR()
        -- end,
        desc = "open ranger",
      },
    },
    config = function()
      local temp_path = "/tmp/togterm"
      require("toggleterm").setup({
        size = function(term)
          if term.direction == "horizontal" then
            return 15
          elseif term.direction == "vertical" then
            return vim.o.columns * 0.3
          end
        end,
        start_in_insert = true,
        float_opts = {
          -- The border key is *almost* the same as 'nvim_open_win'
          -- see :h nvim_open_win for details on borders however
          -- the 'curved' border is a custom border type
          -- not natively supported but implemented in this plugin.
          border = "single", -- 'single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
          -- like `size`, width and height can be a number or function which is passed the current terminal
          width = function(term)
            return math.floor(vim.o.columns * 0.9)
          end,
          height = function(term)
            return math.floor(vim.o.lines * 0.9)
          end,
        },
      })
      
      local Terminal = require("toggleterm.terminal").Terminal

      local lazygit = Terminal:new({
        cmd = "lazygit",
        dir = "git_dir",
        direction = "float",
        float_opts = {
          border = "single", --'single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
          width = function(term)
            return math.floor(vim.o.columns * 0.95)
          end,
          height = function(term)
            return math.floor(vim.o.lines * 0.95)
          end,
        },
        on_open = function(term)
          vim.cmd("startinsert!")
          -- q / <leader>tg 关闭 terminal
          vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", { noremap = true, silent = true })
          vim.api.nvim_buf_set_keymap(
            term.bufnr,
            "n",
            "<leader>tg",
            "<cmd>close<CR>",
            { noremap = true, silent = true }
          )
        end,
        on_close = function(_) end,
      })
      local ranger = Terminal:new({
        cmd = "ranger --choosefiles='" .. temp_path .. "'",
        dir = "git_dir",
        direction = "float",
        float_opts = {
          border = "single",
          width = function(term)
            return math.floor(vim.o.columns * 0.95)
          end,
          height = function(term)
            return math.floor(vim.o.lines * 0.95)
          end,
        },
        on_open = function(term)
          vim.cmd("startinsert!")
          -- q / <leader>tg 关闭 terminal
          vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", { noremap = true, silent = true })
          vim.api.nvim_buf_set_keymap(
            term.bufnr,
            "n",
            "<leader>tr",
            "<cmd>close<CR>",
            { noremap = true, silent = true }
          )
        end,
        on_close = function(_)
          vim.notify("ranger close")
          print("range close")
          local file = io.open(temp_path, "r")
          if file ~= nil then
            local file_path = file:read("*a")
            file:close()
            vim.cmd("silent! vsplit | e " .. file_path)
            os.remove(temp_path)
          end
        end,
      })

      local ta = Terminal:new({
        direction = "float",
        close_on_exit = true,
      })

      local tb = Terminal:new({
        direction = "vertical",
        close_on_exit = true,
      })

      local tc = Terminal:new({
        direction = "horizontal",
        close_on_exit = true,
      })

      local M = {}

      M.toggleA = function()
        if ta:is_open() then
          ta:close()
          return
        end
        tb:close()
        tc:close()
        ta:open()
      end

      M.toggleB = function()
        if tb:is_open() then
          tb:close()
          return
        end
        ta:close()
        tc:close()
        tb:open()
      end

      M.toggleC = function()
        if tc:is_open() then
          tc:close()
          return
        end
        ta:close()
        tb:close()
        tc:open()
      end

      M.toggleR = function()
        ranger:toggle()
      end
      M.toggleG = function()
        lazygit:toggle()
      end
      vim.keymap.set(
        { "n", "t" },
        "<leader>tt",
        "<cmd>ToggleTermToggle<CR>",
        { noremap = true, silent = true, desc = "" }
      )
      vim.keymap.set({ "n", "t" }, "<leader>ta", M.toggleA, { desc = "open float term" })
      vim.keymap.set({ "n", "t" }, "<leader>tb", M.toggleB, { desc = "open right term" })
      vim.keymap.set({ "n", "t" }, "<leader>tc", M.toggleC, { desc = "open bottom term" })
      vim.keymap.set({ "n", "t" }, "<leader>tg", M.toggleG, { desc = "open lazygit" })
      vim.keymap.set({ "n", "t" }, "<leader>gg", M.toggleG, { desc = "open lazygit" })
      vim.keymap.set({ "n", "t" }, "<leader>tr", M.toggleR, { desc = "open ranger" })
    end,
  },
}
