-- 全局提示插件
if vim.g.vscode then
  return;
end
-- 基础配置
require("basic")
-- 快捷键映射
require("keybindings")
-- lazy插件管理
require("plugins")
-- 主题设置
require("colorscheme")
-- 自动命令
require("autocmds")
