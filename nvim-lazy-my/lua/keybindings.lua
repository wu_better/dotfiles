-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

local opts = function(desc)
	return { noremap = true, silent = true, desc = desc }
end
local term_opts = { silent = true }
-- 本地变量
-- local keymap = vim.api.nvim_set_keymap
local function keymap(mode, lhs, rhs, opts)
	opts = opts or {}
	opts.silent = opts.silent ~= false
	vim.keymap.set(mode, lhs, rhs, opts)
end
-- local function keymap(mode, lhs, rhs, opts)
--   local keys = require("lazy.core.handler").handlers.keys
--   ---@cast keys LazyKeysHandler
--   -- do not create the keymap if a lazy keys handler exists
--   if not keys.active[keys.parse({ lhs, mode = mode }).id] then
--     opts = opts or {}
--     opts.silent = opts.silent ~= false
--     vim.keymap.set(mode, lhs, rhs, opts)
--   end
-- end
-- leader key 为空
keymap("", "<Space>", "<Nop>", opts(""))
-- 取消 s 默认功能
keymap("n", "s", "", opts(""))
vim.g.mapleader = " "
vim.g.maplocalleader = " "
-- fix :set wrap
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
-- 窗口相关操作
-- ------------------------------------------------------------------
-- Better window navigation
-- -- alt + hjkl  窗口之间跳转
-- keymap("n", "<A-h>", "<C-w>h", opts(""))
-- keymap("n", "<A-j>", "<C-w>j", opts(""))
-- keymap("n", "<A-k>", "<C-w>k", opts(""))
-- keymap("n", "<A-l>", "<C-w>l", opts(""))
-- keymap("n", "<A-w>", "<C-w>w", opts(""))
-- keymap("n", "<A-=>", "<C-w>=", opts(""))
-- Move to window using the <ctrl> hjkl keys
keymap("n", "<C-h>", "<C-w>h", opts("Go to left window"))
keymap("n", "<C-j>", "<C-w>j", opts("Go to lower window"))
keymap("n", "<C-k>", "<C-w>k", opts("Go to upper window"))
keymap("n", "<C-l>", "<C-w>l", opts("Go to right window"))
-- vim.api.nvim_set_keymap("n", "<C-l>", "<C-w>l", { desc = "Go to right window" })
-- keymap("t", "<Esc>", "<C-\\><C-n>", opts(""))
keymap("t", "<C-h>", [[ <C-\><C-N><C-w>h ]], opts("Go to left window"))
keymap("t", "<C-j>", [[ <C-\><C-N><C-w>j ]], opts("Go to lower window"))
keymap("t", "<C-k>", [[ <C-\><C-N><C-w>k ]], opts("Go to upper window"))
keymap("t", "<C-l>", [[ <C-\><C-N><C-w>l ]], opts("Go to right window"))
keymap("n", "<A-w>", "<C-w>w", opts(""))
keymap("n", "<A-=>", "<C-w>=", opts(""))
-- Resize window using <ctrl> arrow keys
keymap("n", "<C-Up>", "<cmd>resize +2<cr>", opts("Increase window height"))
keymap("n", "<C-Down>", "<cmd>resize -2<cr>", opts("Decrease window height"))
keymap("n", "<C-Left>", "<cmd>vertical resize -2<cr>", opts("Decrease window width"))
keymap("n", "<C-Right>", "<cmd>vertical resize +2<cr>", opts("Increase window width"))

keymap("n", "sv", ":vsp<CR>", opts("")) -- 垂直分割窗口
keymap("n", "sh", ":sp<CR>", opts("")) -- 水平分割窗口
keymap("n", "stt", ":sp | terminal<CR>", opts(""))
keymap("n", "stv", ":vsp | terminal<CR>", opts(""))
keymap("n", "sc", "<C-w>c", opts("close window")) -- 关闭当前
-- 关闭其他
keymap("n", "so", "<C-w>o", opts("close others")) -- close others
-- keymap("t", "sc", [[ <C-\><C-N><C-w>c ]], opts("")) -- 关闭当前
-- -----------窗口end-------------------------------------------------------

keymap("n", "sd", ":TroubleToggle<CR>", opts("")) -- 垂直分割窗口
-- Terminal相关
keymap("n", "sg", ":terminal lazygit<CR>", opts(""))
-- Esc 回 Normal 模式

-- save file
keymap("n", "<leader>w", "<cmd>w<cr><esc>", opts("Save file"))
keymap({ "i", "v", "n", "s" }, "<C-s>", "<cmd>w<cr><esc>", opts("Save file"))
keymap("n", "<leader>q", ":qa!<CR>", opts("not save exit"))
keymap("n", "<leader>o", ":only<CR>", opts("only"))

-- Visual --
-- visual模式下缩进代码
keymap("v", "<", "<gv", opts(""))
keymap("v", ">", ">gv", opts(""))
keymap("n", "<A-h>", "^", opts(""))
keymap("n", "<A-l>", "$", opts(""))
-------------------------------------------------------
-- 上下移动选中文本
-- Move Lines
-- keymap("n", "<A-j>", "<cmd>move .+1<cr>==", opts("Move down"))
-- keymap("n", "<A-k>", "<cmd>move .-2<cr>==", opts("Move up"))
keymap("i", "<A-j>", "<esc><cmd>move .+1<cr>==gi", opts("Move down"))
keymap("i", "<A-k>", "<esc><cmd>move .-2<cr>==gi", opts("Move up"))
keymap("v", "<A-j>", ":move '>+1<cr>gv=gv", opts("Move down"))
keymap("v", "<A-k>", ":move '<-2<cr>gv=gv", opts("Move up"))
-- keymap("v", "<A-j>", ":move .+1<CR>==", opts(""))
-- keymap("v", "<A-k>", ":move .-2<CR>==", opts(""))
-- keymap("v", "J", ":move '>+1<CR>gv-gv", opts(""))
-- keymap("v", "K", ":move '<-2<CR>gv-gv", opts(""))
-- keymap("x", "J", ":move '>+1<CR>gv-gv", opts(""))
-- keymap("x", "K", ":move '<-2<CR>gv-gv", opts(""))
-----------------------------------------------------------
-- keymap("n", "<C-j>", "<cmd>lua require('neoscroll').scroll(5, true, 200)<cr>", opts(""))
-- keymap("n", "<C-k>", "<cmd>lua require('neoscroll').scroll(-5, true, 200)<cr>", opts(""))
-- keymap("n", "<C-k>", "5k", opts(""))
keymap("i", "jj", "<ESC>", opts(""))
keymap("i", "jk", "<ESC>", opts(""))
keymap("i", "<Left>", "<C-G>U<Left>", opts(""))
keymap("i", "<Right>", "<C-G>U<Right>", opts(""))

-- -- $跳到行尾不带空格 (交换$ 和 g_)
-- keymap("v", "$", "g_", opts(""))
-- keymap("v", "g_", "$", opts(""))
-- keymap("n", "$", "g_", opts(""))
-- keymap("n", "g_", "$", opts(""))

-- 在visual mode 里粘贴不要复制
keymap("v", "p", '"_dP', opts(""))
-- magic search
keymap({ "n", "v" }, "/", "/\\v", { noremap = true, silent = false })
-- keymap("v", "/", "/\\v", { noremap = true, silent = false })

-- ctrl + /
keymap({ "n", "v" }, "<C-_>", "gcc", { noremap = false })
-- keymap("v", "<C-_>", "gcc", { noremap = false })

-- insert 模式下，跳到行首行尾
-- keymap("i", "<C-h>", "<ESC>I", opts(""))
-- keymap("i", "<C-l>", "<ESC>A", opts(""))
--------------------------------------------------------------------

-- treesitter 折叠
keymap("n", "zc", ":foldclose<CR>", opts(""))
keymap("n", "zo", ":foldopen<CR>", opts(""))

-- nvim-tree
keymap("n", "<A-m>", ":NvimTreeToggle<CR>", opts(""))
keymap("n", "<leader>m", ":NvimTreeToggle<CR>", opts(""))

----------------------------------------------------------
-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts("next buffer"))
keymap("n", "<S-h>", ":bprevious<CR>", opts("prev buffer"))
keymap("n", "<leader>bc", ":Bdelete!<CR>", opts("buffer delete"))
keymap("n", "<leader>d", ":Bdelete!<CR>", opts("buffer delete"))
-- bufferline
keymap("n", "<leader>bb", ":BufferLinePick<CR>", opts(""))
keymap("n", "<leader>bd", ":BufferLinePickClose<CR>", opts(""))
keymap("t", "<leader>bd", ":BufferLinePickClose<CR>", opts(""))
-- -- "moll/vim-bbye" 关闭当前 buffer
-- -- 关闭左/右侧标签页
keymap("n", "<leader>bh", ":BufferLineCloseLeft<CR>", opts(""))
keymap("n", "<leader>bl", ":BufferLineCloseRight<CR>", opts(""))
-- -- 关闭其他标签页
keymap("n", "<leader>bo", ":BufferLineCloseRight<CR>:BufferLineCloseLeft<CR>", opts(""))
-- -- 关闭选中标签页
--- Re-order to previous/next
keymap("n", "<A-<>", ":BufferLineMovePrev<CR>", opts(""))
keymap("n", "<A->>", ":BufferLineMoveNext<CR>", opts(""))
-- Goto buffer in position...
keymap("n", "<A-0>", ":bfirst<CR>", opts(""))
keymap("n", "<A-1>", ":BufferLineGoToBuffer 1<CR>", opts(""))
keymap("n", "<A-2>", ":BufferLineGoToBuffer 2<CR>", opts(""))
keymap("n", "<A-3>", ":BufferLineGoToBuffe 3<CR>", opts(""))
keymap("n", "<A-4>", ":BufferLineGoToBuffe 4<CR>", opts(""))
keymap("n", "<A-5>", ":BufferLineGoToBuffe 5<CR>", opts(""))
keymap("n", "<A-6>", ":BufferLineGoToBuffe 6<CR>", opts(""))
keymap("n", "<A-7>", ":BufferLineGoToBuffe 7<CR>", opts(""))
keymap("n", "<A-8>", ":BufferLineGoToBuffe 8<CR>", opts(""))
keymap("n", "<A-9>", ":BufferLineGoToBuffe 9<CR>", opts(""))

-- keymap('n', '<A-1>', ':lua require("bufferline").go_to_buffer(1, true)<cr>', opts(""))
-- keymap('n', '<A-2>', ':lua require("bufferline").go_to_buffer(2, true)<cr>', opts(""))
-- keymap('n', '<A-3>', ':lua require("bufferline").go_to_buffer(3, true)<cr>', opts(""))
-- keymap('n', '<A-4>', ':lua require("bufferline").go_to_buffer(4, true)<cr>', opts(""))
-- keymap('n', '<A-5>', ':lua require("bufferline").go_to_buffer(5, true)<cr>', opts(""))
-- keymap('n', '<A-6>', ':lua require("bufferline").go_to_buffer(6, true)<cr>', opts(""))
-- keymap('n', '<A-7>', ':lua require("bufferline").go_to_buffer(7, true)<cr>', opts(""))
-- keymap('n', '<A-8>', ':lua require("bufferline").go_to_buffer(8, true)<cr>', opts(""))
-- keymap('n', '<A-9>', ':lua require("bufferline").go_to_buffer(9, true)<cr>', opts(""))
-- Telescope
keymap("n", "<leader>ff", ":Telescope find_files<CR>", opts("find_files"))
keymap("n", "<leader>fg", ":Telescope live_grep<CR>", opts("live_grep"))
keymap("n", "<leader>fb", ":Telescope buffers<CR>", opts("telescope buffers"))
keymap("n", "<leader>ft", ":Telescope treesitter<CR>", opts("telescope treesitter"))
keymap("n", "<leader>fs", ":Telescope git_status<CR>", opts("telescope git_status"))
keymap("n", "<leader>fd", ":Telescope lsp_definitions<CR>", opts("telescope lsp_definitions"))
keymap("n", "<leader>fr", ":Telescope lsp_references<CR>", opts("telescope lsp_references"))
-- keymap("n", "<leader>fh", ":Telescope help_tags<CR>", opts(""))

-- nvim-tree
-- local function custom_callback(callback_name)
--   return string.format(":lua require('utils.treeutils').%s()<CR>", callback_name)
-- end
-- -- BEGIN_DEFAULT_ON_ATTACH
-- local nvim_tree_opts = function(desc)
--   return { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
-- end
-- vim.keymap.set('n', '<C-f>', custom_callback("launch_find_files"), nvim_tree_opts('search file in current'))
-- vim.keymap.set('n', '<C-g>', custom_callback("launch_live_grep"), nvim_tree_opts('search char'))

-- 列表快捷键
local pluginKeys = {}

--
-- This function has been generated from your
--   view.mappings.list
--   view.mappings.custom_only
--   remove_keymaps
--
-- You should add this function to your configuration and set on_attach = on_attach in the nvim-tree setup call.
--
-- Although care was taken to ensure correctness and completeness, your review is required.
--
-- Please check for the following issues in auto generated content:
--   "Mappings removed" is as you expect
--   "Mappings migrated" are correct
--
-- Please see https://github.com/nvim-tree/nvim-tree.lua/wiki/Migrating-To-on_attach for assistance in migrating.
--
pluginKeys.nvimTree_attach = function(bufnr)
	local api = require("nvim-tree.api")

	local function opts(desc)
		return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
	end

	api.config.mappings.default_on_attach(bufnr)

	-- Default mappings. Feel free to modify or remove as you wish.
	--
	-- BEGIN_DEFAULT_ON_ATTACH
	-- vim.keymap.set('n', '<C-]>', api.tree.change_root_to_node,          opts('CD'))
	-- vim.keymap.set('n', '<C-e>', api.node.open.replace_tree_buffer,     opts('Open: In Place'))
	-- vim.keymap.set('n', '<C-k>', api.node.show_info_popup,              opts('Info'))
	-- vim.keymap.set('n', '<C-r>', api.fs.rename_sub,                     opts('Rename: Omit Filename'))
	-- vim.keymap.set('n', '<C-t>', api.node.open.tab,                     opts('Open: New Tab'))
	-- vim.keymap.set('n', '<C-v>', api.node.open.vertical,                opts('Open: Vertical Split'))
	-- vim.keymap.set('n', '<C-x>', api.node.open.horizontal,              opts('Open: Horizontal Split'))
	-- vim.keymap.set('n', '<BS>',  api.node.navigate.parent_close,        opts('Close Directory'))
	-- vim.keymap.set('n', '<CR>',  api.node.open.edit,                    opts('Open'))
	-- vim.keymap.set('n', '<Tab>', api.node.open.preview,                 opts('Open Preview'))
	-- vim.keymap.set('n', '>',     api.node.navigate.sibling.next,        opts('Next Sibling'))
	-- vim.keymap.set('n', '<',     api.node.navigate.sibling.prev,        opts('Previous Sibling'))
	-- vim.keymap.set('n', '.',     api.node.run.cmd,                      opts('Run Command'))
	-- vim.keymap.set('n', '-',     api.tree.change_root_to_parent,        opts('Up'))
	-- vim.keymap.set('n', 'a',     api.fs.create,                         opts('Create'))
	-- vim.keymap.set('n', 'bmv',   api.marks.bulk.move,                   opts('Move Bookmarked'))
	-- vim.keymap.set('n', 'B',     api.tree.toggle_no_buffer_filter,      opts('Toggle No Buffer'))
	-- vim.keymap.set('n', 'c',     api.fs.copy.node,                      opts('Copy'))
	-- vim.keymap.set('n', 'C',     api.tree.toggle_git_clean_filter,      opts('Toggle Git Clean'))
	-- vim.keymap.set('n', '[c',    api.node.navigate.git.prev,            opts('Prev Git'))
	-- vim.keymap.set('n', ']c',    api.node.navigate.git.next,            opts('Next Git'))
	-- vim.keymap.set('n', 'd',     api.fs.remove,                         opts('Delete'))
	-- vim.keymap.set('n', 'D',     api.fs.trash,                          opts('Trash'))
	-- vim.keymap.set('n', 'E',     api.tree.expand_all,                   opts('Expand All'))
	-- vim.keymap.set('n', 'e',     api.fs.rename_basename,                opts('Rename: Basename'))
	-- vim.keymap.set('n', ']e',    api.node.navigate.diagnostics.next,    opts('Next Diagnostic'))
	-- vim.keymap.set('n', '[e',    api.node.navigate.diagnostics.prev,    opts('Prev Diagnostic'))
	-- vim.keymap.set('n', 'F',     api.live_filter.clear,                 opts('Clean Filter'))
	-- vim.keymap.set('n', 'f',     api.live_filter.start,                 opts('Filter'))
	-- vim.keymap.set('n', 'g?',    api.tree.toggle_help,                  opts('Help'))
	-- vim.keymap.set('n', 'gy',    api.fs.copy.absolute_path,             opts('Copy Absolute Path'))
	-- vim.keymap.set('n', 'H',     api.tree.toggle_hidden_filter,         opts('Toggle Dotfiles'))
	-- vim.keymap.set('n', 'I',     api.tree.toggle_gitignore_filter,      opts('Toggle Git Ignore'))
	-- vim.keymap.set('n', 'J',     api.node.navigate.sibling.last,        opts('Last Sibling'))
	-- vim.keymap.set('n', 'K',     api.node.navigate.sibling.first,       opts('First Sibling'))
	-- vim.keymap.set('n', 'm',     api.marks.toggle,                      opts('Toggle Bookmark'))
	-- vim.keymap.set('n', 'o',     api.node.open.edit,                    opts('Open'))
	-- vim.keymap.set('n', 'O',     api.node.open.no_window_picker,        opts('Open: No Window Picker'))
	-- vim.keymap.set('n', 'p',     api.fs.paste,                          opts('Paste'))
	-- vim.keymap.set('n', 'P',     api.node.navigate.parent,              opts('Parent Directory'))
	-- vim.keymap.set('n', 'q',     api.tree.close,                        opts('Close'))
	-- vim.keymap.set('n', 'r',     api.fs.rename,                         opts('Rename'))
	-- vim.keymap.set('n', 'R',     api.tree.reload,                       opts('Refresh'))
	-- vim.keymap.set('n', 's',     api.node.run.system,                   opts('Run System'))
	-- vim.keymap.set('n', 'S',     api.tree.search_node,                  opts('Search'))
	-- vim.keymap.set('n', 'U',     api.tree.toggle_custom_filter,         opts('Toggle Hidden'))
	-- vim.keymap.set('n', 'W',     api.tree.collapse_all,                 opts('Collapse'))
	-- vim.keymap.set('n', 'x',     api.fs.cut,                            opts('Cut'))
	-- vim.keymap.set('n', 'y',     api.fs.copy.filename,                  opts('Copy Name'))
	-- vim.keymap.set('n', 'Y',     api.fs.copy.relative_path,             opts('Copy Relative Path'))
	-- vim.keymap.set('n', '<2-LeftMouse>',  api.node.open.edit,           opts('Open'))
	-- vim.keymap.set('n', '<2-RightMouse>', api.tree.change_root_to_node, opts('CD'))
	-- END_DEFAULT_ON_ATTACH

	-- Mappings migrated from view.mappings.list
	--
	-- You will need to insert "your code goes here" for any mappings with a custom action_cb
	vim.keymap.set("n", "<C-f>", function()
		-- local node = api.tree.get_node_under_cursor()
		-- your code goes here
		require("utils.treeutils").launch_find_files()
	end, opts("print_the_node_path"))

	vim.keymap.set("n", "<C-g>", function()
		-- local node = api.tree.get_node_under_cursor()
		-- vim.cmd("wincmd l")
		-- your code goes here
		require("utils.treeutils").launch_live_grep()
	end, opts("print_the_node_path"))

	vim.keymap.set("n", "<C-e>", function()
		local node = api.tree.get_node_under_cursor()
		-- your code goes here
		require("utils.executeCommand").execute("code", { node.link_to or node.absolute_path })
	end, opts("print_the_node_path"))

	-- vim.keymap.set('n', '<CR>', api.node.open.edit, opts('Open'))
	-- vim.keymap.set('n', 'o', api.node.open.edit, opts('Open'))
	-- vim.keymap.set('n', '<2-LeftMouse>', api.node.open.edit, opts('Open'))
	-- vim.keymap.set('n', '<C-v>', api.node.open.vertical, opts('Open: Vertical Split'))
	-- vim.keymap.set('n', '<C-x>', api.node.open.horizontal, opts('Open: Horizontal Split'))
	-- vim.keymap.set('n', '<', api.node.navigate.sibling.prev, opts('Previous Sibling'))
	-- vim.keymap.set('n', '>', api.node.navigate.sibling.next, opts('Next Sibling'))
	-- vim.keymap.set('n', 'P', api.node.navigate.parent, opts('Parent Directory'))
	-- vim.keymap.set('n', '<BS>', api.node.navigate.parent_close, opts('Close Directory'))
	-- vim.keymap.set('n', '<Tab>', api.node.open.preview, opts('Open Preview'))
	-- vim.keymap.set('n', 'K', api.node.navigate.sibling.first, opts('First Sibling'))
	-- vim.keymap.set('n', 'J', api.node.navigate.sibling.last, opts('Last Sibling'))
	-- vim.keymap.set('n', 'I', api.tree.toggle_gitignore_filter, opts('Toggle Git Ignore'))
	-- vim.keymap.set('n', 'H', api.tree.toggle_hidden_filter, opts('Toggle Dotfiles'))
	-- vim.keymap.set('n', 'U', api.tree.toggle_custom_filter, opts('Toggle Hidden'))
	-- vim.keymap.set('n', 'R', api.tree.reload, opts('Refresh'))
	-- vim.keymap.set('n', 'a', api.fs.create, opts('Create'))
	-- vim.keymap.set('n', 'd', api.fs.remove, opts('Delete'))
	-- vim.keymap.set('n', 'D', api.fs.trash, opts('Trash'))
	-- vim.keymap.set('n', 'r', api.fs.rename, opts('Rename'))
	-- vim.keymap.set('n', '<C-r>', api.fs.rename_sub, opts('Rename: Omit Filename'))
	-- vim.keymap.set('n', 'x', api.fs.cut, opts('Cut'))
	-- vim.keymap.set('n', 'c', api.fs.copy.node, opts('Copy'))
	-- vim.keymap.set('n', 'p', api.fs.paste, opts('Paste'))
	-- vim.keymap.set('n', 'y', api.fs.copy.filename, opts('Copy Name'))
	-- vim.keymap.set('n', 'Y', api.fs.copy.relative_path, opts('Copy Relative Path'))
	-- vim.keymap.set('n', 'gy', api.fs.copy.absolute_path, opts('Copy Absolute Path'))
	-- vim.keymap.set('n', ']', api.tree.change_root_to_node, opts('CD'))
	-- vim.keymap.set('n', '[', api.tree.change_root_to_parent, opts('Up'))
	-- vim.keymap.set('n', 's', api.node.run.system, opts('Run System'))
	-- vim.keymap.set('n', 'f', api.live_filter.start, opts('Filter'))
	-- vim.keymap.set('n', 'F', api.live_filter.clear, opts('Clean Filter'))
	-- vim.keymap.set('n', 'q', api.tree.close, opts('Close'))
	-- vim.keymap.set('n', 'W', api.tree.collapse_all, opts('Collapse'))
	-- vim.keymap.set('n', 'E', api.tree.expand_all, opts('Expand All'))
	-- vim.keymap.set('n', '.', api.node.run.cmd, opts('Run Command'))
	-- vim.keymap.set('n', '<C-k>', api.node.show_info_popup, opts('Info'))
	-- vim.keymap.set('n', 'g?', api.tree.toggle_help, opts('Help'))
end
pluginKeys.nvimTreeList = function()
	return { -- 打开文件或文件夹
		{
			key = "<C-f>",
			action = "print_the_node_path",
			action_cb = require("utils.treeutils").launch_find_files,
		},
		{
			key = "<C-g>",
			action = "print_the_node_path",
			action_cb = require("utils.treeutils").launch_live_grep,
		},
		{
			key = "<C-e>",
			action = "print_the_node_path",
			action_cb = function(node)
				-- open file by code
				require("utils.executeCommand").execute("code", { node.link_to or node.absolute_path })
			end,
		},
		{ key = { "<CR>", "o", "<2-LeftMouse>" }, action = "edit" },
		-- { key = "O",                              action = "edit_no_picker" },
		-- { key = { "<C-]>", "<2-RightMouse>" },    action = "cd" },
		{ key = "<C-v>", action = "vsplit" },
		{ key = "<C-x>", action = "split" },
		-- { key = "<C-t>",                          action = "tabnew" },
		{ key = "<", action = "prev_sibling" },
		{ key = ">", action = "next_sibling" },
		{ key = "P", action = "parent_node" },
		{ key = "<BS>", action = "close_node" },
		{ key = "<Tab>", action = "preview" },
		{ key = "K", action = "first_sibling" },
		{ key = "J", action = "last_sibling" },
		{ key = "I", action = "toggle_git_ignored" },
		{ key = "H", action = "toggle_dotfiles" },
		{ key = "U", action = "toggle_custom" },
		{ key = "R", action = "refresh" },
		{ key = "a", action = "create" },
		{ key = "d", action = "remove" },
		{ key = "D", action = "trash" },
		{ key = "r", action = "rename" },
		{ key = "<C-r>", action = "full_rename" },
		{ key = "x", action = "cut" },
		{ key = "c", action = "copy" },
		{ key = "p", action = "paste" },
		{ key = "y", action = "copy_name" },
		{ key = "Y", action = "copy_path" },
		{ key = "gy", action = "copy_absolute_path" },
		-- { key = "[e",                             action = "prev_diag_item" },
		-- { key = "[c",                             action = "prev_git_item" },
		-- { key = "]e",                             action = "next_diag_item" },
		-- { key = "]c",                             action = "next_git_item"c },
		-- 进入下一级
		{ key = { "]" }, action = "cd" },
		-- 进入上一级
		{ key = { "[" }, action = "dir_up" }, -- 进入上一级
		-- { key = "-",                              action = "dir_up" }, -- 进入上一级
		{ key = "s", action = "system_open" }, -- 文件管理器打开
		{ key = "f", action = "live_filter" },
		{ key = "F", action = "clear_live_filter" },
		{ key = "q", action = "close" },
		{ key = "W", action = "collapse_all" },
		{ key = "E", action = "expand_all" },
		-- { key = "S",                              action = "search_node" },
		{ key = ".", action = "run_file_command" },
		{ key = "<C-k>", action = "toggle_file_info" },
		{ key = "g?", action = "toggle_help" },
		-- { key = "m",                              action = "toggle_mark" },
		-- { key = "bmv",                            action = "bulk_move" },
	}
end
-- Telescope 列表中 插入模式快捷键
pluginKeys.telescopeList = function(actions, layout)
	return {
		i = {
			-- 上下移动
			["<C-j>"] = "move_selection_next",
			["<C-k>"] = "move_selection_previous",
			["<C-n>"] = "move_selection_next",
			["<C-p>"] = "move_selection_previous",
			-- 历史记录
			["<Down>"] = "cycle_history_next",
			["<Up>"] = "cycle_history_prev",
			-- 关闭窗口
			-- ["<esc>"] = actions.close,
			["<C-c>"] = actions.close --[[ "close" ]],
			-- 预览窗口上下滚动
			["<C-u>"] = "preview_scrolling_up",
			["<C-d>"] = "preview_scrolling_down",
			["<C-v>"] = layout.toggle_preview,
		},
		n = {
			["<C-c>"] = actions.close --[[ "close" ]],
			["<C-v>"] = layout.toggle_preview,
		},
	}
end

-- 代码注释插件
-- see ./lua/plugin-config/comment.lua
pluginKeys.comment = {
	-- Normal 模式快捷键
	toggler = {
		line = "gcc", -- 行注释
		block = "gbc", -- 块注释
	},
	-- Visual 模式
	opleader = {
		line = "gc",
		bock = "gb",
	},
}

-- lsp 回调函数快捷键设置
pluginKeys.mapLSP = function(mapbuf)
	-- hover
	mapbuf("n", "gh", "<cmd>Lspsaga hover_doc<CR>", opts("hover doc"))
	-- mapbuf("n", "gh", "<cmd>lua vim.lsp.buf.hover()<CR>", opts(""))

	-- rename
	mapbuf("n", "<leader>cr", "<cmd>Lspsaga rename<CR>", opts("rename"))
	-- mapbuf("n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts(""))

	-- code action
	mapbuf("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts("code action"))
	-- mapbuf("n", "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts(""))

	mapbuf("n", "<leader>cd", "<cmd>Lspsaga goto_definition<CR>", opts(""))
	mapbuf("n", "<leader>cp", "<cmd>Lspsaga peek_definition<CR>", opts("peek definition"))
	mapbuf("n", "<leader>co", "<cmd>Lspsaga outline<CR>", opts("outline"))
	mapbuf("n", "<leader>ct", "<cmd>Lspsaga term_toggle<CR>", opts("term toggle"))

	-- mapbuf("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts(""))
	mapbuf(
		"n",
		"gd",
		"<cmd>lua require'telescope.builtin'.lsp_definitions({ initial_mode = 'normal', })<CR>",
		opts("go to defined")
	)

	-- Lspsaga 替换 gr
	mapbuf("n", "gr", "<cmd>Lspsaga lsp_finder<CR>", opts("lsp finder"))
	-- mapbuf("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts(""))
	--[[
  Lspsaga 替换 gp, gj, gk
  mapbuf("n", "gp", "<cmd>lua vim.diagnostic.open_float()<CR>", opts(""))
  mapbuf("n", "gj", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts(""))
  mapbuf("n", "gk", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts(""))
  --]]
	-- diagnostic
	mapbuf("n", "<leader>sd", "<cmd>Lspsaga show_line_diagnostics<CR>", opts("show line diagnostics"))
	mapbuf("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<cr>", opts("diagnostics jump next"))
	mapbuf("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<cr>", opts("diagnostics jump prev"))
	mapbuf("n", "sf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", opts("format"))
	mapbuf("v", "sf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", opts("format"))
	-- 未用
	-- mapbuf("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts(""))
	-- mapbuf("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts(""))
	-- mapbuf('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts(""))
	-- mapbuf("n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts(""))
	-- mapbuf('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts(""))
	-- mapbuf('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts(""))
	-- mapbuf('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts(""))
	-- mapbuf('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts(""))
end

-- typescript 快捷键
pluginKeys.mapTsLSP = function(mapbuf)
	mapbuf("n", "gs", ":TSLspOrganize<CR>", opts(""))
	mapbuf("n", "gR", ":TSLspRenameFile<CR>", opts(""))
	mapbuf("n", "gi", ":TSLspImportAll<CR>", opts(""))
end

-- nvim-dap
pluginKeys.mapDAP = function()
	keymap(
		"n",
		"<leader>de",
		":lua require'dap'.close()<CR>"
			.. ":lua require'dap'.terminate()<CR>"
			.. ":lua require'dap.repl'.close()<CR>"
			.. ":lua require'dapui'.close()<CR>"
			.. ":lua require('dap').clear_breakpoints()<CR>"
			.. "<C-w>o<CR>",
		opts("")
	)
	-- 继续
	keymap("n", "<leader>dc", ":lua require'dap'.continue()<CR>", opts(""))
	-- 设置断点
	keymap("n", "<leader>dt", ":lua require('dap').toggle_breakpoint()<CR>", opts(""))
	keymap("n", "<leader>dT", ":lua require('dap').clear_breakpoints()<CR>", opts(""))
	--  stepOver, stepOut, stepInto
	keymap("n", "<leader>dj", ":lua require'dap'.step_over()<CR>", opts(""))
	keymap("n", "<leader>dk", ":lua require'dap'.step_out()<CR>", opts(""))
	keymap("n", "<leader>dl", ":lua require'dap'.step_into()<CR>", opts(""))
	-- 弹窗
	keymap("n", "<leader>dh", ":lua require'dapui'.eval()<CR>", opts(""))
end

-- nvim-cmp 自动补全
pluginKeys.cmp = function(cmp)
	local feedkey = function(key, mode)
		vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
	end
	local has_words_before = function()
		local line, col = unpack(vim.api.nvim_win_get_cursor(0))
		return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
	end
	local luasnip = require("luasnip")

	return {
		-- 上一个
		["<C-k>"] = cmp.mapping.select_prev_item(),
		-- 下一个
		["<C-j>"] = cmp.mapping.select_next_item(),
		-- 出现补全
		["<A-o>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
		-- 取消
		["<A-c>"] = cmp.mapping({
			i = cmp.mapping.abort(),
			c = cmp.mapping.close(),
		}),
		-- 确认
		-- Accept currently selected item. If none selected, `select` first item.
		-- Set `select` to `false` to only confirm explicitly selected items.
		["<CR>"] = cmp.mapping.confirm({
			select = true,
			behavior = cmp.ConfirmBehavior.Replace,
		}),
		-- ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
		-- 如果窗口内容太多，可以滚动
		["<C-u>"] = cmp.mapping(cmp.mapping.scroll_docs(-3), { "i", "c" }),
		["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(3), { "i", "c" }),
		-- snippets 站位符跳转
		-- ["<A-l>"] = cmp.mapping(function(_)
		--   if vim.fn["vsnip#available"](1) == 1 then
		--     feedkey("<Plug>(vsnip-expand-or-jump)", "")
		--   end
		-- end, { "i", "s" }),
		-- ["<A-h>"] = cmp.mapping(function()
		--   if vim.fn["vsnip#jumpable"]( -1) == 1 then
		--     feedkey("<Plug>(vsnip-jump-prev)", "")
		--   end
		-- end, { "i", "s" }),
		-- -- super Tab
		-- ["<Tab>"] = cmp.mapping(function(fallback)
		--   if cmp.visible() then
		--     cmp.select_next_item()
		--   elseif vim.fn["vsnip#available"](1) == 1 then
		--     feedkey("<Plug>(vsnip-expand-or-jump)", "")
		--   elseif has_words_before() then
		--     cmp.complete()
		--   else
		--     fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
		--   end
		-- end, { "i", "s" }),
		-- ["<S-Tab>"] = cmp.mapping(function()
		--   if cmp.visible() then
		--     cmp.select_prev_item()
		--   elseif vim.fn["vsnip#jumpable"]( -1) == 1 then
		--     feedkey("<Plug>(vsnip-jump-prev)", "")
		--   end
		-- end, { "i", "s" }),
		-- end of super Tab
		["<A-l>"] = cmp.mapping(function(fallback)
			if luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<A-h>"] = cmp.mapping(function(fallback)
			if luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			-- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable()
			-- they way you will only jump inside the snippet region
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}
end

-- -- 自定义 toggleterm 3个不同类型的命令行窗口
-- -- <leader>ta 浮动
-- -- <leader>tb 右侧
-- -- <leader>tc 下方
-- -- 特殊lazygit 窗口，需要安装lazygit
-- -- <leader>tg lazygit
-- -- <leader>tr ranger
pluginKeys.mapToggleTerm = function(toggleterm)
	vim.keymap.set({ "n", "t" }, "<leader>tt", "<cmd>ToggleTermToggle<CR>", opts(""))
	vim.keymap.set({ "n", "t" }, "<leader>ta", toggleterm.toggleA, { desc = "open float term" })
	vim.keymap.set({ "n", "t" }, "<leader>tb", toggleterm.toggleB, { desc = "open right term" })
	vim.keymap.set({ "n", "t" }, "<leader>tc", toggleterm.toggleC, { desc = "open bottom term" })
	vim.keymap.set({ "n", "t" }, "<leader>tg", toggleterm.toggleG, { desc = "open lazygit" })
	vim.keymap.set({ "n", "t" }, "<leader>gg", toggleterm.toggleG, { desc = "open lazygit" })
	vim.keymap.set({ "n", "t" }, "<leader>tr", toggleterm.toggleR, { desc = "open ranger" })
end

-- gitsigns
pluginKeys.gitsigns_on_attach = function(bufnr)
	local gs = package.loaded.gitsigns

	local function map(mode, l, r, opts)
		opts = opts or {}
		opts.buffer = bufnr
		vim.keymap.set(mode, l, r, opts)
	end

	-- Navigation
	map("n", "]c", function()
		if vim.wo.diff then
			return "]c"
		end
		vim.schedule(function()
			gs.next_hunk()
		end)
		return "<Ignore>"
	end, { expr = true, desc = "next_hunk" })

	map("n", "[c", function()
		if vim.wo.diff then
			return "[c"
		end
		vim.schedule(function()
			gs.prev_hunk()
		end)
		return "<Ignore>"
	end, { expr = true, desc = "prev_hunk" })

	--   -- Actions
	map({ "n", "v" }, "<leader>hs", ":Gitsigns stage_hunk<CR>")
	map({ "n", "v" }, "<leader>hr", ":Gitsigns reset_hunk<CR>")
	map("n", "<leader>hS", gs.stage_buffer, { desc = "stage_buffer" })
	map("n", "<leader>hu", gs.undo_stage_hunk, { desc = "undo_stage_hunk" })
	map("n", "<leader>hR", gs.reset_buffer, { desc = "reset_buffer" })
	map("n", "<leader>hp", gs.preview_hunk, { desc = "preview_hunk" })
	map("n", "<leader>hb", function()
		gs.blame_line({ full = true })
	end, { desc = "blame_line" })
	-- map('n', '<leader>tb', gs.toggle_current_line_blame)
	map("n", "<leader>hl", gs.toggle_current_line_blame, { desc = "toggle_current_line_blame" })
	map("n", "<leader>hd", gs.diffthis, { desc = "diffthis" })
	map("n", "<leader>hD", function()
		gs.diffthis("~")
	end, { desc = "diffthis~" })
	map("n", "<leader>ht", gs.toggle_deleted, { desc = "toggle_deleted" })

	-- Text object
	map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>")
end

return pluginKeys
