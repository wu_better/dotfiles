-- 自动安装 lazy.nvim
-- 插件安装目录
-- ~/.local/share/nvim/lazy/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.notify("正在安装lazy.nvim，请稍后...")
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)



-- Use a protected call so we don't error out on first use
local status_ok, lazy = pcall(require, "lazy")
if not status_ok then
  vim.notify("lazy.nvim install error", "error")
  return
end

local plugins = {
  -- 主题start
  { "navarasu/onedark.nvim", },
  { "folke/tokyonight.nvim", },
  { "catppuccin/nvim",           name = "catppuccin" },
  { "EdenEast/nightfox.nvim" }, -- lazy
  { "olimorris/onedarkpro.nvim", },
  { "RRethy/nvim-base16",        lazy = true },
  { "sainnhe/gruvbox-material" },
  -- 主题end
  {
    "kyazdani42/nvim-tree.lua",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      require("plugin-config.nvim-tree")
    end,
  },
  {
    'akinsho/bufferline.nvim',
    tag = "v3.0.0",
    event = "BufEnter",
    dependencies = { 'nvim-tree/nvim-web-devicons', "famiu/bufdelete.nvim" },
    config = function()
      require("plugin-config.bufferline")
    end
  },
  {
    'nvim-lualine/lualine.nvim',
    event = "VeryLazy",
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function()
      require("plugin-config.lualine")
    end
  },
  -- 搜索
  {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.1',
    -- or
    -- branch = '0.1.1',
    dependencies = { 'nvim-lua/plenary.nvim', 'nvim-telescope/telescope-ui-select.nvim' },
    config = function()
      require("plugin-config.telescope")
    end
  },
  {
    "nvim-treesitter/nvim-treesitter",
    event = { "BufReadPost", "BufNewFile" },
    -- event = "BufEnter",
    dependencies = {
      'nvim-treesitter/nvim-treesitter-refactor',
    },
    build = function()
      require("nvim-treesitter.install").update({ with_sync = true })
    end,
    -- build = ":TSUpdate",
    config = function()
      require("plugin-config.nvim-treesitter")
    end
  },
  {
    "hiphish/rainbow-delimiters.nvim",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
    },
    config = function()
      -- This module contains a number of default definitions
      local rainbow_delimiters = require 'rainbow-delimiters'

      vim.g.rainbow_delimiters = {
        strategy = {
          [''] = rainbow_delimiters.strategy['global'],
          vim = rainbow_delimiters.strategy['local'],
        },
        query = {
          [''] = 'rainbow-delimiters',
          lua = 'rainbow-blocks',
        },
        highlight = {
          'RainbowDelimiterRed',
          'RainbowDelimiterYellow',
          'RainbowDelimiterBlue',
          'RainbowDelimiterOrange',
          'RainbowDelimiterGreen',
          'RainbowDelimiterViolet',
          'RainbowDelimiterCyan',
        },
      }
    end
  },
  {
    "windwp/nvim-ts-autotag",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
    },
    config = function()
      require('nvim-ts-autotag').setup()
    end
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    event = { "BufReadPost", "BufNewFile" },
    -- event = "VeryLazy",
    -- event = { "BufReadPost", "BufNewFile" },
    config = function()
      require("plugin-config.indent-blankline")
    end
  },
  {
    'numToStr/Comment.nvim',
    event = "VeryLazy",
    config = function()
      require('plugin-config.comment')
    end
  },
  {
    "hrsh7th/nvim-cmp",
    -- event = "VeryLazy",
    -- event = "InsertEnter",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp", -- { name = nvim_lsp }
      "hrsh7th/cmp-buffer",   -- { name = 'buffer' },
      "hrsh7th/cmp-path",     -- { name = 'path' }
      "hrsh7th/cmp-cmdline",  -- { name = 'cmdline' }
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',
      --片段补全
      -- "hrsh7th/cmp-vsnip", -- {name = vsnip} 片段补全
      -- -- Snippet 引擎
      -- "hrsh7th/vim-vsnip",
      -- 常见编程语言代码段
      "rafamadriz/friendly-snippets",
      -- UI 增强
      "onsails/lspkind-nvim",
    },
    config = function()
      require('lsp.cmp')
    end
  },
  -- LSP config
  {
    "williamboman/mason.nvim",
    dependencies = {
      "williamboman/mason-lspconfig.nvim",
      "neovim/nvim-lspconfig",
      "folke/neodev.nvim", -- lua lsp 增强
    },
    config = function()
      require("lsp")
    end
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    -- lazy = true,
    event = "VeryLazy",
    -- event = { "BufReadPre", "BufNewFile" },
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      require("lsp.null-ls")
    end
  },
  -- typescript 增强
  { "jose-elias-alvarez/typescript.nvim" },
  -- lsp 进程显示
  {
    "j-hui/fidget.nvim",
    lazy = true,
    config = function()
      require("plugin-config.fidget")
    end
  },
  {
    "glepnir/lspsaga.nvim",
    branch = "main",
    event = "LspAttach",
    -- event = "VeryLazy",
    -- commit = "438b54cba00fca27d280ae4d9242615282045bcb",
    -- commit = "ba8ad94b42a9a807c2ab0b4545c098f0b513f3f4",
    -- commit = "bc20f696ae01e93e08db3fd725648b164e69d3e0",
    commit = "4f075452c466df263e69ae142f6659dcf9324bf6",
    dependencies = { "nvim-tree/nvim-web-devicons", "nvim-treesitter/nvim-treesitter" },
    config = function()
      require("lsp.lspsaga")
    end,
  },
  -- trouble诊断
  {
    "folke/trouble.nvim",
    event = "VeryLazy",
    cmd = { "TroubleToggle", "Trouble" },
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      require("plugin-config.trouble")
    end
  },
  -- 终端
  {
    'akinsho/toggleterm.nvim',
    version = "*",
    event = "VeryLazy",
    config = function()
      require("plugin-config.toggleterm")
    end
  },
  --git
  {
    'lewis6991/gitsigns.nvim',
    event = "VeryLazy",
    -- event = { "BufReadPre", "BufNewFile" },
    -- tag = 'release' -- To use the latest release (do not use this if you run Neovim nightly or dev builds!)
    config = function()
      require("plugin-config.gitsigns")
    end
  },
  {
    "windwp/nvim-autopairs",
    -- event = 'InsertCharPre',
    event = "BufEnter",
    config = function()
      require("plugin-config.autopairs")
    end
  },
  -- surround
  {
    "kylechui/nvim-surround",
    event = "BufEnter",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
      require("plugin-config.nvim-surround")
    end
  },
  -- noice
  {
    "folke/noice.nvim",
    event = "VeryLazy",
    dependencies = {
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
      -- OPTIONAL:
      --   `nvim-notify` is only needed, if you want to use the notification view.
      --   If not available, we use `mini` as the fallback
      "rcarriga/nvim-notify",
    },
    config = function()
      require("plugin-config.noice")
    end
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      require("plugin-config.which-key")
    end,
  },
  {
    "karb94/neoscroll.nvim",
    event = 'WinScrolled',
    config = function()
      require('neoscroll').setup({ hide_cursor = false })
    end,
  },
  {
    "ggandor/leap.nvim",
    -- event = "VeryLazy",
    keys = {
      { "s",  mode = { "n", "x", "o" }, desc = "Leap forward to" },
      { "S",  mode = { "n", "x", "o" }, desc = "Leap backward to" },
      { "gs", mode = { "n", "x", "o" }, desc = "Leap from windows" },
    },
    dependencies = {
      -- easily jump to any location and enhanced f/t motions for Leap
      {
        "ggandor/flit.nvim",
        keys = function()
          ---@type LazyKeys[]
          local ret = {}
          for _, key in ipairs({ "f", "F", "t", "T" }) do
            ret[#ret + 1] = { key, mode = { "n", "x", "o" }, desc = key }
          end
          return ret
        end,
        opts = { labeled_modes = "nx" },
      },

    },
    config = function(_, opts)
      local leap = require("leap")
      for k, v in pairs(opts) do
        leap.opts[k] = v
      end
      leap.add_default_mappings(true)
      vim.keymap.del({ "x", "o" }, "x")
      vim.keymap.del({ "x", "o" }, "X")
    end,
  },
  {
    'mg979/vim-visual-multi',
    event = "VeryLazy",
    -- event = { "BufReadPost", "BufNewFile" },
    branch = 'master'
  },
  {
    "jackMort/ChatGPT.nvim",
    event = "VeryLazy",
    config = function()
      require("plugin-config.chatgpt");
    end,
    dependencies = {
      "MunifTanjim/nui.nvim",
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope.nvim"
    }
  }
}
-- require("lazy").setup({
--   spec = {
--     -- add LazyVim and import its plugins
--     -- { "LazyVim/LazyVim", import = "lazyvim.plugins" },
--     -- import any extras modules here
--     -- { import = "lazyvim.plugins.extras.lang.typescript" },
--     -- { import = "lazyvim.plugins.extras.lang.json" },
--     -- { import = "lazyvim.plugins.extras.ui.mini-animate" },
--     -- import/override with your plugins
--     { import = "plugins" },
--   },
--   defaults = {
--     -- By default, only LazyVim plugins will be lazy-loaded. Your custom plugins will load during startup.
--     -- If you know what you're doing, you can set this to `true` to have all your custom plugins lazy-loaded by default.
--     lazy = false,
--     -- It's recommended to leave version=false for now, since a lot the plugin that support versioning,
--     -- have outdated releases, which may break your Neovim install.
--     version = false, -- always use the latest git commit
--     -- version = "*", -- try installing the latest stable version for plugins that support semver
--   },
--   -- install = { colorscheme = { "tokyonight", "habamax" } },
--   checker = { enabled = true }, -- automatically check for plugin updates
--   performance = {
--     rtp = {
--       -- disable some rtp plugins
--       disabled_plugins = {
--         "gzip",
--         -- "matchit",
--         -- "matchparen",
--         -- "netrwPlugin",
--         "tarPlugin",
--         "tohtml",
--         "tutor",
--         "zipPlugin",
--       },
--     },
--   },
-- })
lazy.setup(plugins)
