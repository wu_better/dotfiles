local status, chatgpt = pcall(require, "chatgpt")
if not status then
  vim.notify("没有找到 nvim-tree")
  return
end
local home = vim.fn.expand("$HOME")
chatgpt.setup({  -- BEGIN_DEFAULT_OPTS
  api_key_cmd = "echo $OPENAI_API_KEY"
})               -- END_DEFAULT_OPTS
