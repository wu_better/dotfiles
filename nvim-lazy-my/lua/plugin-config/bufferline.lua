local status, bufferline = pcall(require, "bufferline")
if not status then
  vim.notify("没有找到 bufferline")
  return
end
-- bfferline 配置
-- https://github.com/akinsho/bufferline.nvim#configuration
bufferline.setup({
  highlights = {
    buffer_selected = {
      fg = '#E06C75',
      bg = nil,
      bold = true,
      italic = true,
    },
  },
  options = {
    mode = "buffers",
    -- always_show_bufferline = false,
    -- 关闭 Tab 的命令
    -- close_command = "bdelete! %d",
    close_command = function(bufnum)
      require('bufdelete').bufdelete(bufnum, true)
    end,
    -- right_mouse_command = "bdelete! %d",
    right_mouse_command = function(bufnum)
      require('bufdelete').bufdelete(bufnum, true)
    end,
    numbers = function(opts)
      -- return string.format('%s·%s', opts.raise(opts.id), opts.lower(opts.ordinal))
      return string.format('%s', opts.raise(opts.ordinal))
    end,
    modified_icon = "",
    left_trunc_marker = '',
    right_trunc_marker = '',
    separator_style = "thin",     --"slant" | "thick" | "thin" | { 'any', 'any' },
    -- 侧边栏配置
    -- 左侧让出 nvim-tree 的位置，显示文字 File Explorer
    offsets = {
      {
        filetype = "NvimTree",
        text = "File Explorer",
        text_align = "left",
      },
    },
    hover = {
      enabled = true,
      delay = 200,
      reveal = { 'close' }
    },
    -- custom_areas = {
    --   right = function()
    --     local result = {}
    --     local seve = vim.diagnostic.severity
    --     local error = #vim.diagnostic.get(0, { severity = seve.ERROR })
    --     local warning = #vim.diagnostic.get(0, { severity = seve.WARN })
    --     local hint = #vim.diagnostic.get(0, { severity = seve.HINT })
    --     local info = #vim.diagnostic.get(0, { severity = seve.INFO })
    --
    --     if error ~= 0 then
    --       table.insert(result, { text = "  " .. error, fg = "#e55561" })
    --     end
    --
    --     if warning ~= 0 then
    --       table.insert(result, { text = "  " .. warning, fg = "#e2b86b" })
    --     end
    --
    --     if hint ~= 0 then
    --       table.insert(result, { text = "  " .. hint, fg = "#bf68d9" })
    --       -- 
    --     end
    --
    --     if info ~= 0 then
    --       table.insert(result, { text = "  " .. info, fg = "#4fa6ed" })
    --     end
    --     return result
    --   end,
    -- }
    --[[ -- 使用 nvim 内置 LSP  后续课程会配置
    diagnostics = "nvim_lsp",
    -- 可选，显示 LSP 报错图标
    ---@diagnostic disable-next-line: unused-local
    diagnostics_indicator = function(count, level, diagnostics_dict, context)
      local s = " "
      for e, n in pairs(diagnostics_dict) do
        local sym = e == "error" and " " or (e == "warning" and " " or "")
        s = s .. n .. sym
      end
      return s
    end, ]]
  },
})
