local status, fidget = pcall(require, "fidget")
if not status then
  vim.notify("没有找到 fidget")
  return
end

fidget.setup({
  text = {
    spinner = "pipe",        -- animation shown when tasks are ongoing
    done = "✔",            -- character shown when all tasks are complete
    commenced = "Started",   -- message shown when task starts
    completed = "Completed", -- message shown when task completes
  },
  window = {
    relative = "win", -- where to anchor, either "win" or "editor"
    blend = 0,        -- &winblend for the window
    zindex = 8,       -- the zindex value for the window
    border = "none",  -- style of border for the fidget window
  },
  sources = {
    -- ["null-ls"] = {
    --   ignore = true,
    -- },
  },
})
