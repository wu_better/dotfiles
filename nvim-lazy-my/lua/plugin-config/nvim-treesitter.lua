local status, treesitter = pcall(require, "nvim-treesitter.configs")
if not status then
  vim.notify("没有找到 nvim-treesitter")
  return
end

--    colors = {
--      "#95ca60",
--      "#ee6985",
--      "#D6A760",
--      "#7794f4",
--      "#b38bf5",
--      "#7cc7fe",
--    }, -- table of hex strings
treesitter.setup({
  -- 安装 language parser
  -- :TSInstallInfo 命令查看支持的语言
  ensure_installed = { "json", "html", "css", "vim", "lua", "javascript", "typescript", "tsx", "vue", "bash", "regex",
    "markdown",
    "markdown_inline" },
  -- ensure_installed = "maintained",

  -- 启用代码高亮模块
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
    disable = function(lang, buf)
      local max_filesize = 100 * 1024 -- 100 KB
      local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
      if ok and stats and stats.size > max_filesize then
        return true
      end
    end,
  },
  -- 启用增量选择模块
  -- incremental_selection = {
  --   enable = false,
  --   keymaps = {
  --     init_selection = "<CR>",
  --     node_incremental = "<CR>",
  --     node_decremental = "<BS>",
  --     scope_incremental = "<TAB>",
  --   },
  -- },
  -- 启用代码缩进模块 (=)
  indent = {
    enable = true,
  },
  -- require 'nvim-treesitter/nvim-treesitter-refactor'
  -- refactor = {
  --   highlight_definitions = {
  --     enable = true,
  --     -- Set to false if you have an `updatetime` of ~100.
  --     clear_on_cursor_move = true,
  --   },
  --   highlight_current_scope = {
  --     enable = false,
  --   },
  -- },
})
vim.cmd('highlight TSRainbowRed  guifg=#95ca60 ctermfg=White')
vim.cmd('highlight TSRainbowYellow  guifg=#ee6985 ctermfg=White')
vim.cmd('highlight TSRainbowBlue  guifg=#d6a760 ctermfg=White')
vim.cmd('highlight TSRainbowOrange  guifg=#7794f4 ctermfg=White')
vim.cmd('highlight TSRainbowGreen  guifg=#b38bf5 ctermfg=White')
vim.cmd('highlight TSRainbowViolet  guifg=#8cc8fe ctermfg=White')
vim.cmd('highlight TSRainbowCyan  guifg=#d65d0e ctermfg=White')
-- 开启 Folding 模块
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
-- 默认不要折叠
-- https://stackoverflow.com/questions/8316139/how-to-set-the-default-to-unfolded-when-you-open-a-file
vim.opt.foldlevel = 99
