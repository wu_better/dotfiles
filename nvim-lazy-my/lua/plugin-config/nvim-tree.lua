local status, nvim_tree = pcall(require, "nvim-tree")
if not status then
  vim.notify("没有找到 nvim-tree")
  return
end
-- 列表操作快捷键
-- local list_keys = require("keybindings").nvimTreeList
nvim_tree.setup {     -- BEGIN_DEFAULT_OPTS
  auto_reload_on_write = true,
  disable_netrw = false,
  hijack_cursor = false,
  hijack_netrw = true,
  hijack_unnamed_buffer_when_opening = false,
  sort_by = "name",
  root_dirs = {},
  prefer_startup_root = false,
  sync_root_with_cwd = false,
  reload_on_bufenter = false,
  respect_buf_cwd = false,
  on_attach = require("keybindings").nvimTree_attach,
  select_prompts = false,
  view = {
    centralize_selection = false,
    cursorline = true,
    debounce_delay = 15,
    width = 30,
    -- hide_root_folder = false,
    side = "left",
  },
  renderer = {
    add_trailing = false,
    group_empty = false,
    highlight_git = true,
    full_name = false,
    highlight_opened_files = "none",
    highlight_modified = "none",
    root_folder_label = ":~:s?$?/..?",
    indent_width = 2,
    icons = {
      webdev_colors = true,
      git_placement = "before",
      modified_placement = "after",
      padding = " ",
      symlink_arrow = " ➛ ",
      show = {
        file = true,
        folder = true,
        folder_arrow = true,
        git = true,
        modified = true,
      },
    },
    special_files = { "Cargo.toml", "Makefile", "README.md", "readme.md" },
    symlink_destination = true,
  },
  update_focused_file = {
    enable = true,
    update_root = false,
    ignore_list = {},
  },
  system_open = {
    cmd = "thunar",
    args = {},
  },
  filters = {
    dotfiles = true,
    git_clean = false,
    no_buffer = false,
    custom = {},
    exclude = {},
  },
  actions = {
    use_system_clipboard = true,
    change_dir = {
      enable = true,
      global = false,
      restrict_above_cwd = false,
    },
    expand_all = {
      max_folder_discovery = 300,
      exclude = {},
    },
  },
  trash = {
    cmd = "gio trash",
  },
}     -- END_DEFAULT_OPTS

local function open_nvim_tree(data)
  -- buffer is a [No Name]
  local no_name = data.file == "" and vim.bo[data.buf].buftype == ""

  -- buffer is a directory
  local directory = vim.fn.isdirectory(data.file) == 1

  -- if not no_name and not directory then
  --   return
  -- end

  -- change to the directory
  if directory then
    vim.cmd.cd(data.file)
    -- open the tree
    require("nvim-tree.api").tree.open()
  end
end
vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })
-- https://github.com/kyazdani42/nvim-tree.lua
-- view.mappings.list = { -- BEGIN_DEFAULT_MAPPINGS
--   { key = { "<CR>", "o", "<2-LeftMouse>" }, action = "edit" },
--   { key = "<C-e>",                          action = "edit_in_place" },
--   { key = "O",                              action = "edit_no_picker" },
--   { key = { "<C-]>", "<2-RightMouse>" },    action = "cd" },
--   { key = "<C-v>",                          action = "vsplit" },
--   { key = "<C-x>",                          action = "split" },
--   { key = "<C-t>",                          action = "tabnew" },
--   { key = "<",                              action = "prev_sibling" },
--   { key = ">",                              action = "next_sibling" },
--   { key = "P",                              action = "parent_node" },
--   { key = "<BS>",                           action = "close_node" },
--   { key = "<Tab>",                          action = "preview" },
--   { key = "K",                              action = "first_sibling" },
--   { key = "J",                              action = "last_sibling" },
--   { key = "I",                              action = "toggle_git_ignored" },
--   { key = "H",                              action = "toggle_dotfiles" },
--   { key = "U",                              action = "toggle_custom" },
--   { key = "R",                              action = "refresh" },
--   { key = "a",                              action = "create" },
--   { key = "d",                              action = "remove" },
--   { key = "D",                              action = "trash" },
--   { key = "r",                              action = "rename" },
--   { key = "<C-r>",                          action = "full_rename" },
--   { key = "x",                              action = "cut" },
--   { key = "c",                              action = "copy" },
--   { key = "p",                              action = "paste" },
--   { key = "y",                              action = "copy_name" },
--   { key = "Y",                              action = "copy_path" },
--   { key = "gy",                             action = "copy_absolute_path" },
--   { key = "[e",                             action = "prev_diag_item" },
--   { key = "[c",                             action = "prev_git_item" },
--   { key = "]e",                             action = "next_diag_item" },
--   { key = "]c",                             action = "next_git_item" },
--   { key = "-",                              action = "dir_up" },
--   { key = "s",                              action = "system_open" },
--   { key = "f",                              action = "live_filter" },
--   { key = "F",                              action = "clear_live_filter" },
--   { key = "q",                              action = "close" },
--   { key = "W",                              action = "collapse_all" },
--   { key = "E",                              action = "expand_all" },
--   { key = "S",                              action = "search_node" },
--   { key = ".",                              action = "run_file_command" },
--   { key = "<C-k>",                          action = "toggle_file_info" },
--   { key = "g?",                             action = "toggle_help" },
--   { key = "m",                              action = "toggle_mark" },
--   { key = "bmv",                            action = "bulk_move" },
-- } -- END_DEFAULT_MAPPINGS
