local keybindings = require("keybindings")
local opts = {
  flags = {
    debounce_text_changes = 150,
  },
  capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities()),
  -- https://github.com/jose-elias-alvarez/nvim-lsp-ts-utils/blob/main/lua/nvim-lsp-ts-utils/utils.lua
  -- 传入 tsserver 初始化参数
  -- make inlay hints work
  init_options = {
    hostInfo = "neovim",
    preferences = {
      includeInlayParameterNameHints = "all",
      importModuleSpecifierPreference = 'project=relative',
      -- jsxAttributeCompletionStyle = 'auto',
      includeInlayParameterNameHintsWhenArgumentMatchesName = true,
      includeInlayFunctionParameterTypeHints = true,
      includeInlayVariableTypeHints = true,
      includeInlayPropertyDeclarationTypeHints = true,
      includeInlayFunctionLikeReturnTypeHints = true,
      includeInlayEnumMemberValueHints = true,
    },
  },
  on_attach = function(client, bufnr)
    -- 禁用格式化功能，交给专门插件插件处理
    -- client.server_capabilities.documentFormattingProvider = false -- 0.8 and later
    -- if client.name == "tsserver" then
    --   client.resolved_capabilities.document_formatting = false -- 0.7 and earlier
    --   client.server_capabilities.documentFormattingProvider = false -- 0.8 and later
    -- end
    -- client.server_capabilities.document_formatting = false
    -- client.server_capabilities.document_range_formatting = false
    local function buf_set_keymap(...)
      vim.api.nvim_buf_set_keymap(bufnr, ...)
    end
    -- 绑定快捷键
    keybindings.mapLSP(buf_set_keymap)
    -- no default maps, so you may want to define some here
    keybindings.mapTsLSP(buf_set_keymap)
  end,
}

return {
  on_setup = function(server)
    require("typescript").setup({
      server = opts,
    })
    -- server.setup(opts)
  end,
}
