local cmp = require("cmp")
require("luasnip.loaders.from_vscode").lazy_load();
require("luasnip.loaders.from_vscode").load({ paths = "~/.config/nvim/my_snippets" })
cmp.setup({
  -- 指定 snippet 引擎
  snippet = {
    expand = function(args)
      -- For `vsnip` users.
      -- vim.fn["vsnip#anonymous"](args.body)
      -- For `luasnip` users.
      require('luasnip').lsp_expand(args.body)
      -- For `ultisnips` users.
      -- vim.fn["UltiSnips#Anon"](args.body)
      -- For `snippy` users.
      -- require'snippy'.expand_snippet(args.body)
    end,
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  experimental = {
    ghost_text = true
  },
  -- 来源
  sources = cmp.config.sources({
    {
      name = "nvim_lsp",
      entry_filter = function(entry)
        return require('cmp.types').lsp.CompletionItemKind[entry:get_kind()] ~= 'Text'
      end
    },
    -- For vsnip users.
    -- { name = "vsnip" },
    -- For luasnip users.
    { name = 'luasnip' },
    --For ultisnips users.
    -- { name = 'ultisnips' },
    -- -- For snippy users.
    -- { name = 'snippy' },
    { name = "buffer" },
  }, { { name = "path" } }),
  -- 快捷键
  mapping = require("keybindings").cmp(cmp),
  -- 使用lspkind-nvim显示类型图标
  formatting = require("lsp.ui").formatting,
})

-- Use buffer source for `/`.
cmp.setup.cmdline({ '/', '?' }, {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

-- Use cmdline & path source for ':'.
cmp.setup.cmdline(":", {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = "path" },
  }, {
    { name = "cmdline" },
  }),
})
vim.cmd([[
set completeopt=menuone,noinsert,noselect
highlight! default link CmpItemKind CmpItemMenuDefault
]])
