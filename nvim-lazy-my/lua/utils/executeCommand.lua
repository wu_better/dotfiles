local M = {}
M.execute = function(cmd, args)
  if #cmd == 0 then
    vim.notify.warn "Cannot execute command. Unrecognized platform."
    return
  end
  local process = {
    cmd = cmd,
    args = args,
    errors = "\n",
    stderr = vim.loop.new_pipe(false),
  }
  -- table.insert(process.args, node.link_to or node.absolute_path)
  process.handle, process.pid = vim.loop.spawn(
    process.cmd,
    { args = process.args, stdio = { nil, nil, process.stderr }, detached = true },
    function(code)
      process.stderr:read_stop()
      process.stderr:close()
      process.handle:close()
      if code ~= 0 then
        notify.warn(string.format("execute command failed with return code %d: %s", code, process.errors))
      end
    end
  )
  table.remove(process.args)
  if not process.handle then
    notify.warn(string.format("execute command failed to spawn command '%s': %s", process.cmd, process.pid))
    return
  end
  vim.loop.read_start(process.stderr, function(err, data)
    if err then
      return
    end
    if data then
      process.errors = process.errors .. data
    end
  end)
  vim.loop.unref(process.handle)
end

return M
