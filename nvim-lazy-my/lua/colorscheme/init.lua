require("colorscheme.tokyonight").setup()
-- require("colorscheme.onedarkpro").setup()
-- require("colorscheme.onedark").setup()
-- require("colorscheme.base16").setup()
-- require("colorscheme.gruvbox").setup()
-- require("colorscheme.catppuccin").setup()



vim.cmd('highlight Pmenu ctermfg=0 ctermbg=13 guifg=#a0a8b7 guibg=None');
vim.cmd('highlight NormalFloat guifg=#a0a8b7 guibg=None');
vim.cmd('highlight FloatBorder guifg=#98C379 guibg=None')

vim.cmd('highlight CodeActionNumber guifg=#c678dd guibg=None')
-- unused var hl to comment
vim.cmd('highlight link DiagnosticUnnecessary  Comment')


vim.cmd('highlight WinSeparator guifg=gray')
vim.cmd('highlight NvimTreeWinSeparator guifg=gray')
