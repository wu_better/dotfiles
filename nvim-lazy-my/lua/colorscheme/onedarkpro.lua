local M = {}
function M.setup()
  require("onedarkpro").setup({
    caching = false,                                                    -- Enable caching
    cache_path = vim.fn.expand(vim.fn.stdpath("cache") .. "/onedarkpro"), -- The path to the cache directory
    cache_suffix = "_compiled",
    colors = {},                                                        -- Add/override colors
    highlights = {},                                                    -- Add/override highlights
    styles = {
      types = "NONE",                                                   -- Style that is applied to types
      methods = "NONE",                                                 -- Style that is applied to methods
      numbers = "NONE",                                                 -- Style that is applied to numbers
      strings = "NONE",                                                 -- Style that is applied to strings
      comments = "italic",                                                -- Style that is applied to comments
      keywords = "NONE",                                                -- Style that is applied to keywords
      constants = "NONE",                                               -- Style that is applied to constants
      functions = "italic",                                               -- Style that is applied to functions
      operators = "NONE",                                               -- Style that is applied to operators
      variables = "NONE",                                               -- Style that is applied to variables
      parameters = "NONE",                                              -- Style that is applied to parameters
      conditionals = "NONE",                                            -- Style that is applied to conditionals
      virtual_text = "italic",                                            -- Style that is applied to virtual text
    },
    options = {
      cursorline = false,               -- Use cursorline highlighting?
      transparency = false,            -- Use a transparent background?
      terminal_colors = true,          -- Use the theme's colors for Neovim's :terminal?
      highlight_inactive_windows = true, -- When the window is out of focus, change the normal background?
    },
  })
  -- onedark onelight onedark_vivid onedark_dark

  local colorscheme = "onedark"
  -------------------------------------------------------------------------------
  local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
  if not status_ok then
    vim.notify("colorscheme: " .. colorscheme .. " 没有找到！")
    return
  end
end
return M
