local M = {}
function M.setup()
  -- material mix original
  vim.g.gruvbox_material_foreground = "material"
  vim.o.background = "dark" --light
  -- hard medium(default) soft
  vim.g.gruvbox_material_background = 'hard'
  -- For better performance
  -- vim.g.gruvbox_material_better_performance = 1
  -- vim.g.gruvbox_material_disable_italic_comment = 0 --default 0
  vim.g.gruvbox_material_enable_bold = 0   --default 0
  vim.g.gruvbox_material_enable_italic = 1 --default 0

  --Customize the cursor color, only works in GUI clients.
  --    Available values:   `'auto'`, `'red'`, `'orange'`, `'yellow'`, `'green'`,
  --  `'aqua'`, `'blue'`, `'purple'`
  -- vim.g.gruvbox_material_cursor = 'auto'

  -- To use transparent background, set this option to `1`.
  -- If you want more ui components to be transparent (for example, status line
  -- background), set this option to `2`.
  vim.g.gruvbox_material_transparent_background = 0
  vim.g.gruvbox_material_dim_inactive_windows = 0 --default 0
  vim.g.gruvbox_material_visual = 'grey background'

  vim.cmd [[colorscheme gruvbox-material]]
end

return M
