## 设置字体大小

setfont ter-132n

## 一、检查引导方式

检测当前引导模式是否是 UEFI

```sh
ls /sys/firmware/efi/efivars
```

若有输出目录则为efi引导启动

## 二、联网

### 检查网卡

执行如下命令查看网卡是否被列出

```sh
 ip -brief link
#1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
#    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
#2: enp0s31f6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
#    link/ether xx:xx:xx:xx:xx:xx brd ff:ff:ff:ff:ff:ff
#3: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DORMANT group default qlen 1000
#    link/ether xx:xx:xx:xx:xx:xx brd ff:ff:ff:ff:ff:ff
```

- lo 虚拟网卡
- e 开头的 为 有线网卡
- w 开头的 为 无线网卡

### 有线链接

确保e开头的网卡是否为开启状态 上方输出中的`UP`

### 无线连接

使用iwd的命令行模式进行wifi连接

```sh
iwctl #进入无线网络配置
device list #列出设备名称 如wlan0
station wlan0 scan #扫描网络
station wlan0 get-networks #列出网络
station wlan0 connect 网络名称  #进行链接 输入密码
exit
```

### 测试网络连通性

```sh
ping www.baidu.com
```

## 三、同步系统时间

```sh
timedatectl set-ntp true
```

## 四、磁盘分区

| 分区名称 | 大小   | 挂载点 |
| -------- | ------ | ------ |
| sda1     | 500M   | efi    |
| sda2     | 8G     | swap   |
| sda3     | 剩下的 | /      |

```sh
fdisk -l #查看磁盘列表
#对sda 盘进行分区
fdisk /dev/sda
#进入分区
```

g 创建gpt 分区表  
n 创建分区  
p 查看分区

```sh
cat /proc/partitions #查看分区表刷新了没有 若没有我们刚才创建的分区表 则 需要刷新：
partprobe /dev/sda
```

## 五、格式化分区

```sh
mkfs.fat -F32 /dev/sda1     #格式化 FAT
mkswap /dev/sda2            #格式化  swap
mkfs.ext4 /dev/sda3         #格式化 ext4 文件系统
swapon /dev/sda2            #激活swap分区
```

## 六、挂载文件系统

```sh
#必须先挂载根分区
mount /dev/sda3 /mnt #挂载根分区
mkdir /mnt/boot
mkdir /mnt/boot/EFI
mount /dev/sda1 /mnt/boot/EFI
```

## 七、修改镜像源

由于国外镜像访问慢，这里需要修改国内源

- 修改源

```sh
vim /etc/pacman.d/mirrorlist
```

/ustc 回车 查找ustc 中科大源  
V 选中2行文本  
d 剪切  
g0 返回到文件开头
找个空行  
p 粘贴上去  
:wq 保存退出
或者
过滤官方镜像列表中的前 5 个镜像，按速度排列并覆盖 /etc/pacman.d/mirrorlist  
reflector -l 5 --sort rate --save /etc/pacman.d/mirrorlist  
下面这个命令会从官方镜像列表中获取200个最近同步过的源，并对这200个源进行大文件下载来，根据在你电脑里的下载速度进行排序，写入mirrorlist（强烈推荐）

reflector --verbose -l 200 -p http --sort rate --save /etc/pacman.d/mirrorlist

Server = https://mirrors.tuna.tsinghua.edu.cn/archlinux/$repo/os/$arch  
Server = http://mirrors.163.com/archlinux/$repo/os/$arch

## 七、在线安装基础包

```sh

pacstrap /mnt base base-devel linux linux-firmware  sudo  vim openssh man dhcpcd iwd networkmanager bash-completion git
```

- \*base 基础系统包 包括iprouter2等工具包
- base-devel 开发人员基础工具包,包括一些开发人员需要的用的工具，编译工具 如`automake`,`cmake` 之类的
- \*linux 内核
- \*linux-firmware 内核框架
- sudo
- vim 编辑器
- openssh ssh服务
- man 帮助手册
- dhcpcd dhcp服务 自动获取ip地址
- iwd 无线网管理
- networkmanager 网络管理器
- bash-completion 命令行自动补全

其中\*号 为必选项

## 八、生成fstab文件

系统开机自动挂载硬盘

```sh
 genfstab -U /mnt >> /mnt/etc/fstab
 #检查分区表 是否正常
 cat /mnt/etc/fstab
```

## 九、将根更改为新系统

```sh
 arch-chroot /mnt
```

## 十、本地化

### 1.设置主机名

```sh
vim /etc/hostname
```

### 2.设置hosts

编辑 /etc/hosts文件，添加以下内容

<pre>
127.0.0.1    localhost.localdomain    localhost
::1          localhost.localdomain    localhost
127.0.1.1    xxxx.localdomain     xxxx
</pre>

### 3.更改时区

```sh
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 运行hwclock 生成 ：/etc/adjtime
hwclock --systohc --utc  #同步硬件时间
```

### 4.进行本地语言设置

编辑 /etc/locale.gen

```sh
vim /etc/locale.gen
#删除 en_US.UTF-8 UTF-8 和 zh_CN.UTF-8 UTF-8 前的#

#以生成 locale 讯息
locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf
```

### 5.修改root密码

```sh
passwd root
#输入密码
```

### 6.添加用户

```sh
# arch 是要设置的用户名，可以更改为自己想要的用户名
useradd -m -G wheel -s /bin/bash arch
# 为你的用户设置密码
passwd arch
# 输入密码

#修改用户arch 权限
visudo
#搜索%wheel 找到 #%wheel ALL=(ALL) ALL
#删除#号
```

### 5.添加国内修改源

```sh
vim /etc/pacman.conf
# 搜Color 去除# 命令行出错返回彩色
# 去掉 #[multilib] 这两行去掉#

```

最后添加上

<pre>
[archlinuxcn]
Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch
Server = http://mirrors.163.com/archlinux-cn/$arch
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
</pre>

```sh
pacman -Syyu
pacman -S archlinuxcn-keyring
#如果报错执行如下命令
rm -rf /etc/pacman.d/gnupg
pacman-key --init
pacman-key --populate archlinux
pacman-key --populate archlinuxcn
```

## 十一、安装并配置grub2引导

- uefi 引导

```sh
pacman -S grub efibootmgr intel-ucode
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB

grub-mkconfig -o /boot/grub/grub.cfg
```

- bios 引导

```sh
pacman -S grub –-noconfirm
grub-install --target=i386-pc /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
```

## 十二、字体

```sh
#安装中文字体（Dejavu 和 微米黑字体）
pacman -S adobe-source-han-serif-cn-fonts adobe-source-han-sans-cn-fonts wqy-zenhei  wqy-microhei noto-fonts-cjk noto-fonts-emoji noto-fonts-extra ttf-dejavu
#图标字体
pacman -S ttf-font-awesome
#可读取ntfs格式的磁盘
pacman -S ntfs-3g
音频设备没声音
sudo pacman -S sof-firmware alsa-firmware alsa-tools pulseaudio-alsa

蓝牙驱动
sudo pacman -S bluez bluez-utils
#设置开机启动项（也可以之后开启）
systemctl enable sshd NetworkManager iwd dhcpcd
```

## 十三、退出重启

```sh
exit
umount -R /mnt
reboot
```

## 十四、登录管理器dm

- dm

```sh
sudo pacman -S lightdm  lightdm-gtk-greeter
sudo systemctl enable lightdm
```

- ly TUI 界面

```sh
sudo pacman -S yay
yay -S ly-git
sudo systemctl enable ly
```

## 十五、图形界面

### 基于xorg

```sh
sudo pacman -S xorg-server //安装XServer
pacman -S network-manager-applet //i3托盘
sudo pacman -S xdg-user-dirs
```

- xfce4

```sh
sudo pacman -S xfce4 xfce4-goodies
```

- gnome

```sh
sudo pacman -S gnome-shell gnome-shell-extensions gnome-control-center gnome-terminal gnome-tweaks nautilus gdm
```

- kde

```sh
sudo pacman -S plasma-desktop plasma-pa konsole kscreen dolphin discover sddm
# 选择2）phonon-qt5-vlc
```

- i3

```sh
sudo pacman -S i3-gaps i3lock polybar picom feh rofi ranger lxappearance  //安装i3wm,锁屏,状态栏,背景透明 背景图，启动菜单 主题设置

```

- dwm

### 基于wayland

- sway

```
pacman -S sway xorg-xwayland waybar swaylock swayidle swaybg wofi
pacman -S terminator alacritty
```

- hyprland

```
yay -S hyprland
pacman -S terminator alacritty
```

登录进去之后执行 startx 就会启动i3

## 十四、安装fcitx5 输入法

```sh
sudo pacman -S fcitx5-im fcitx5-chinese-addons fcitx5-pinyin-zhwiki fcitx5-material-color
```

编辑 `/etc/environment`
添加

<pre>
GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
XMODIFIERS=@im=fcitx
INPUT_METHOD=fcitx
SDL_IM_MODULE=fcitx
</pre>

重启之后进行设置
执行 `fcitx5-configtool` 取消勾选only show current language 选择pinyin 到左边保存

## 十五、

---

---

1.安装显卡驱动

查看显卡
lspci | grep VGA

pacman -S 驱动包

| 显卡       | 驱动包             |
| ---------- | ------------------ |
| 通用       | xf86-video-vesa    |
| intel-     | xf86-video-intel   |
| amdgpu     | xf86-video-amdgpu  |
| Geforce7+- | xf86-video-nouveau |
| Geforce6/7 | xf86-video-304xx   |
| ati        | xf86-video-ati     |

amd  
pacman -S mesa lib32-mesa xf86-video-amdgpu vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau

2.触摸板驱动（笔记本用）  
pacman -S xf86-input-synaptics

字体  
sudo pacman -S ttf-consolas-with-yahei  
sudo pacman -S wqy-bitmapfont

软件  
sudo pacman -S flameshot gvfs  
yay -S netease-cloud-music wps-office  
sudo pacman -S virtualbox virtualbox-guest-iso  
选2：virtualbox-host-modules-arch

Creating group 'vboxusers' with GID 108  
virtualbox 的可选依赖  
 vde2: Virtual Distributed Ethernet support  
 virtualbox-guest-iso: Guest Additions CD image  
 virtualbox-ext-vnc: VNC server support  
 virtualbox-sdk: Developer kit

sudo usermod -a -G vboxusers wyj

polybar 主题  
git clone --depth=1 https://github.com/adi1090x/polybar-themes.git

为ranger 添加图标

git clone https://github.com/cdump/ranger-devicons2 ~/.config/ranger/plugins/devicons2  
添加自体布丁  
git clone --depth=1 https://github.com/ryanoasis/nerd-fonts.git

虚拟机{}

```sh
sudo pacman -S open-vm-tools
sudo pacman -S gtkmm
sudo pacman -S xf86-video-vmware
sudo pacman -S xf86-input-vmmouse
systemctl enable vmtoolsd

```

pacman -S thunar-archive-plugin xarchiver zip unzip p7zip arj lzop cpio unrar

qq 微信调节dpi

```sh
env WINEPREFIX="$HOME/.deepinwine/Deepin-TIM" deepin-wine5 winecfg
env WINEPREFIX="$HOME/.deepinwine/Deepin-WeChat" deepin-wine6-stable winecfg
```

autotiling
i3-master-layout
i3-alternating-layout
