#!/usr/bin/env bash

# Add this script to your wm startup file.
# Terminate already running bar instances
killall -q waybar
# Wait until the processes have been shut down
while pgrep -u $UID -x waybar >/dev/null; do sleep 1; done

# Launch the bar
# env GTK_DEBUG=interactive 
waybar
